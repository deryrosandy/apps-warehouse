<?php

/**
 * Check if Result Query more or one row
 *
 * @param unknown $query        	
 * @return
 *
 */
function checkRes($query) {
	if ($query->num_rows() > 0) {
		return $query->result();
	} else {
		return false;
	}
}

/**
 * Check if Result Query has one row
 *
 * @param unknown $query        	
 * @return boolean
 */
function checkRow($query) {
	if ($query->num_rows() > 0) {
		return $query->row();
	} else {
		return false;
	}
}

/**
 * Count Row
 *
 * @param unknown $query        	
 * @return boolean
 */
function countRow($query) {
	if ($query->num_rows() > 0) {
		return $query->num_rows();
	} else {
		return false;
	}
}

/**
 * Only Number Allow
 *
 * @param unknown $num        	
 * @return mixed
 */
function numberOnly($num) {
	return preg_replace('/\D/', '', $num);
}

// MSG
function show_msg($content='', $type='success', $icon='fa-info-circle', $size='14px') {
	if ($content != '') {
				
		return  '<div class="alert alert-' .$type .'" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<strong class="d-block d-sm-inline-block-force"><i class="icon fa ' .$icon .'"></i></strong>  ' .$content . '!
				</div>';
	}
}

function show_succ_msg($content='', $size='14px') {
	if ($content != '') {
		return  '<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="success" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<strong class="d-block d-sm-inline-block-force"><i class="icon fa fa-check"></i></strong>  ' .$content . '!
				</div>';
	}
}

function show_err_msg($content='', $size='14px') {
	if ($content != '') {
		return  '<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<strong class="d-block d-sm-inline-block-force"><i class="icon fa fa-info-circle"></i></strong>  ' .$content . '!
				</div>';
	}
}

// MODAL
function show_my_modal($content='', $id='', $data='', $size='md') {
	$_ci = &get_instance();

	if ($content != '') {
		$view_content = $_ci->load->view($content, $data, TRUE);

		return '<div class="modal fade" id="' .$id .'" role="dialog">
				  <div class="modal-dialog modal-' .$size .'" role="document">
					<div class="modal-content">
						' .$view_content .'
					</div>
				  </div>
				</div>';
	}
}

function show_my_confirm($id='', $class='', $title='Konfirmasi', $yes = 'Ya', $no = 'Tidak') {
	$_ci = &get_instance();

	if ($id != '') {
		echo   '<div class="modal fade" id="' .$id .'" role="dialog">
				  <div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
						  <h3 style="display:block; text-align:center;">' .$title .'</h3>
						  
						  <div class="col-md-6">
							<button class="form-control btn btn-primary ' .$class .'"> <i class="glyphicon glyphicon-ok-sign"></i> ' .$yes .'</button>
						  </div>
						  <div class="col-md-6">
							<button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> ' .$no .'</button>
						  </div>
						</div>
					</div>
				  </div>
				</div>';
	}
}

function tgl_indo($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function bulan_tahun($tgl){
		$bulan = getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $bulan.' '.$tahun;		 
}

function getBulan($bln){
	switch ($bln){
		case 1: 
			return "Januari";
			break;
		case 2:
			return "Februari";
			break;
		case 3:
			return "Maret";
			break;
		case 4:
			return "April";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Juni";
			break;
		case 7:
			return "Juli";
			break;
		case 8:
			return "Agustus";
			break;
		case 9:
			return "September";
			break;
		case 10:
			return "Oktober";
			break;
		case 11:
			return "November";
			break;
		case 12:
			return "Desember";
			break;
	}
}

function tgl_indo2($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan2(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function getHour($hour){
	$jam = substr($hour,10,6);
	return $jam;	
}

function getBulan2($bln){
	switch ($bln){
		case 1: 
			return "Jan";
			break;
		case 2:
			return "Feb";
			break;
		case 3:
			return "Mar";
			break;
		case 4:
			return "Apr";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Jun";
			break;
		case 7:
			return "Jul";
			break;
		case 8:
			return "Ags";
			break;
		case 9:
			return "Sep";
			break;
		case 10:
			return "Okt";
			break;
		case 11:
			return "Nov";
			break;
		case 12:
			return "Des";
			break;
	}
}

function getHari($tgl){

	$day = date('D', strtotime($tgl));

	if ($day=='Sun'){
		return 'Minggu';
	}elseif($day=='Mon'){
		return 'Senin';
	}elseif($day=='Tue'){
		return 'Selasa';
	}elseif($day=='Wed'){
		return 'Rabu';
	}elseif($day=='Thu'){
		return 'Kamis';
	}elseif($day=='Fri'){
		return 'Jumat';
	}elseif($day=='Sat'){
		return 'Sabtu';
	}else{
		return 'ERROR!';
	}
}

function format_rupiah($angka){
	if($angka != null || $angka != '0'){
		$rupiah = "Rp. " . number_format($angka,0,',','.');
	}else{
		$rupiah = $angka;
	}
	return $rupiah;
}

function format_digit($angka){
	if($angka == '0' || $angka == null || $angka == '-'){
		return '- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}else{
		$digit = number_format(str_replace(' ', '', $angka),0,',','.');
		return $digit;
	}
}

function format_decimal($angka){
	$digit = number_format($angka,2,',','.');
	return $digit;
}

function get_level_therapist($levelkomisi){
	switch ($levelkomisi) {
		case "1":
			return 'Therapist Junior';
			break;
		case "2":
			return 'Therapist Senior';
			break;
		case "3":
			return 'Therapist Lama';
			break;
	}
}

function get_level_therapist_short($levelkomisi){
	switch ($levelkomisi) {
		case "1":
			return 'JR';
			break;
		case "2":
			return 'SR';
			break;
		case "3":
			return 'TL';
			break;
	}
}

function get_vendor_name($vendor_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$vendor_name = $CI->M_General->get_vendor_by_id($vendor_id)->vendor_name;
	
	return $vendor_name;
}
function get_user_name($user_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$first_name = $CI->M_General->get_user_by_id($user_id)->first_name;
	$last_name 	= $CI->M_General->get_user_by_id($user_id)->last_name;
	
	return $first_name;
}

function get_customer_name($customer_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$customer_name = $CI->M_General->get_customer_by_id($customer_id)->customer_name;
	
	return $customer_name;
}
function get_category_name($category_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$category_name = $CI->M_General->get_category_by_id($category_id)->category_name;
	
	return $category_name;
}
function get_warehouse_name($warehouse_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$warehouse_name = $CI->M_General->get_warehouse_by_id($warehouse_id)->warehouse_name;
	
	return $warehouse_name;
}

function get_therapist($kodekar){
	$CI = get_instance();
	$CI->load->model('M_General');

	$therapist = $CI->M_General->get_therapist_by_kodekar($branch_id);

	if($therapist!==null){
		return $therapist;
	}else{
		return "Therapist Not Available";
	}
}

function get_supplier_name($supplier_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$supplier_name = $CI->M_General->get_supplier_name_by_id($supplier_id);
	if($supplier_name!==null){
		return $supplier_name;
	}else{
		return "Supplier Not Set";
	}
}

function get_kodetrp($kodekar){
	$CI = get_instance();
	$CI->load->model('M_General');

	$kodetrp = $CI->M_General->get_kodetrp_by_kodekar($kodekar);
	
	if($kodetrp!==null){
		return $kodetrp;
	}else{
		return "-";
	}
}

function get_status_update($kodekar){
	$month = date('Y-m');
	$CI = get_instance();
	$CI->load->model('M_General');
	//var_dump($month); die();
	$data = $CI->M_General->get_therapist_performance_by_kodekar_month($kodekar, $month);
	
	if($data==null){
		return "<span class='badge badge-warning pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Not Yet</span>";
	}else{
		return "<span class='badge badge-success pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Done</span>";		
	}
}

function get_level_spv($id_spv){
	switch ($id_spv) {
		case "0":
			return 'SPV Therapist';
			break;
		case "1":
			return 'Ibu Mess';
			break;
	}
}

function get_status_color($status){
	switch ($status){
		case "open": 
		return "<span class='badge badge-warning pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Open</span>";
			break;
		case "process":
		return "<span class='badge badge-info pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>ON Process</span>";
			break;
		case "done":
		return "<span class='badge badge-success pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Completed</span>";
			break;
	}
}

function get_status_yesno($status){
	switch ($status){
		case "0": 
		return "Tidak";
			break;
		case "1":
		return "Ya";
			break;
	}
}
function get_status_kulit($status){
	switch ($status){
		case "1": 
		return "Kuning Langsat";
			break;
		case "2":
		return "Cokelat";
			break;
		case "3":
		return "Gelap";
			break;
	}
}
function get_month_name($month){
	$month = substr($month,5);
	switch ($month){
		case "01": 
		return "JAN";
			break;
		case "02":
		return "FEB";
			break;
		case "03":
		return "MAR";
			break;
		case "04":
		return "APR";
			break;
		case "05":
		return "MEI";
			break;
		case "06":
		return "JUN";
			break;
		case "07":
		return "JUL";
			break;
		case "08":
		return "AGS";
			break;
		case "09":
		return "SEPT";
			break;
		case "10":
		return "OKT";
			break;
		case "11":
		return "NOV";
			break;
		case "12":
		return "DES";
			break;
	}
}
function get_status_rambut($status){
	switch ($status){
		case "1": 
		return "Sehat";
			break;
		case "2":
		return "Ketombe";
			break;
		case "3":
		return "Pecah";
			break;
	}
}
function get_status_penampilan($status){
	switch ($status){
		case "0": 
		return "OW";
			break;
		case "1":
		return "Ideal";
			break;
	}
}

function get_status_ketamu($status){
	switch ($status){
		case "0": 
		return "Jutek";
			break;
		case "1":
		return "Menyenangkan";
			break;
	}
}
function get_status_kerekan($status){
	switch ($status){
		case "0": 
		return "Cuek";
			break;
		case "1":
		return "Bekerjasama";
			break;
	}
}

function get_status_active($status){
	switch ($status){
		case "1": 
		return "<span class='badge badge-success pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Active</span>";
			break;
		case "0":
		return "<span class='badge badge-danger pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Non Active</span>";
			break;
	}
}

function get_status_therapist($status){
	switch ($status){
		case "active": 
		return "<span class='badge  badge-success pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Active</span>";
			break;
		case "cuti":
		return "<span class='badge  badge-warning pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Cuti</span>";
			break;
		case "kabur":
		return "<span class='badge  badge-danger pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Kabur</span>";
			break;
		case "pelatihan":
		return "<span class='badge  badge-warning pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Pelatihan</span>";
			break;
		case "magang":
		return "<span class='badge  badge-warning pd-x-10 pd-y-5 tx-white tx-11 tx-roboto'>Magang</span>";
			break;
	}
}

function get_level_user($level){
	switch ($level){
		case "supervisor": 
			return "SPV Outlet";
			break;
		case "admin": 
			return "Admin";
			break;
		case "superadmin": 
			return "Super Admin";
			break;
	}
}

function get_gross_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('M_General');
	$CI->load->model('M_Komisi_lulur');
	//Cari komisi Lulur
	$total_sesi = $CI->M_General->get_total_session_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);

	$data_point = $CI->M_General->get_total_sesi_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal);

	
	
	$kodetrp 	=  $CI->M_General->get_kodetrp_by_kodekar_makasar($kodekar);
	
	$levelkomisi 	=  $CI->M_General->get_level_komisi_by_kodekar($kodekar);
	//var_dump($levelkomisi); 
	$skema_komisi	 =  $CI->M_Komisi_lulur->get_skema_komisi_lulur($data_point->total_point, $levelkomisi);	
	$total_komisi_premium = (($total_sesi->total_pre*10000)+($total_sesi->total_prs*10000));
	//var_dump($total_komisi_premium); die();
	$total_komisi_lulur = ($total_sesi->total_sesi * $skema_komisi);

	//Cari komisi Add
	$total_add = $CI->M_General->get_total_additional_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
	//Cari Komisi Lain-lain
	$komisi_lain_lain = $CI->M_General->get_komisi_lain_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);

	//Cari Komisi FNB
	$total_fnb = $CI->M_General->get_komisi_fnb_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);

	//Cari Komisi Kuesioner
	//$total_kuesioner = $CI->M_General->get_komisi_kuesioner_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar);

	if($komisi_lain_lain !== null){
		$a = intval($komisi_lain_lain->jumlah_komisi);
	}else{
		$a = 0;
	}

	$b = intval($total_komisi_lulur)+intval($total_komisi_premium);
	$d = intval($total_fnb->jumlah_komisi);
	//$e = intval($total_kuesioner->jumlah_komisi);

	if($total_add !== null){
		$c = intval($total_add->total_komisi_additional);
	}else{
		$c = 0;
	}
	
	$total_gaji = $a+$b+$c+$d;
	
	return $total_gaji;
}

function get_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$gross_total = get_gross_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	//var_dump($gross_total); die();
	$potongan_gaji = get_potongan_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$total_kuesioner = get_kuesioner($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
	$tabungan_temp = get_tabungan($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$tabungan = ($tabungan_temp !== null ? (intVal($tabungan_temp->setoran)) : '-');
	
	$total_potongan = $potongan_gaji->yayasan+$potongan_gaji->asuransi+$potongan_gaji->koperasi+$potongan_gaji->mess+$potongan_gaji->hutang+$potongan_gaji->denda+$potongan_gaji->zion+$potongan_gaji->lain_lain+$potongan_gaji->trt_glx+$potongan_gaji->bpjs_kry+$potongan_gaji->sertifikasi+$potongan_gaji->tiket;
	$total_gaji = intval($gross_total)+intval($total_kuesioner)-intval($total_potongan)-intval($tabungan);
	//var_dump($total_potongan); die();
	return $total_gaji;
}

function get_takehomepay($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$gross_total = get_gross_total_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$potongan_gaji = get_potongan_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$total_kuesioner = get_kuesioner($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
	$komisi_kodes = get_komisi_kodes($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	$total_gaji = intval($gross_total)+intval($total_kuesioner)+intval($potongan_gaji->bpjs_prs)+intval($komisi_kodes);
	//var_dump($total_potongan); die();
	return $total_gaji;
}

function get_kuesioner($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$total_kuesioner = $CI->M_General->get_komisi_kuesioner_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id);
	//var_dump($total_kuesioner);
	return $total_kuesioner;
}

function get_potongan_gaji($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('M_General');
	$potongan = $CI->M_General->get_potongan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
	return $potongan;
}

function get_komisi_kodes($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	$CI = get_instance();
	$CI->load->model('M_General');
	$komisi_kodes = $CI->M_General->get_komisi_kodes_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	
	return $komisi_kodes;
}

function get_tabungan($kodekar, $dari_tanggal, $sampai_tanggal,  $branch_id){
	$CI = get_instance();
	$CI->load->model('M_General');
	$tabungan = $CI->M_General->get_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
	//var_dump($tabungan); die();
	return $tabungan;
}

?>