<style type="text/css">
	html { margin: 30px;margin-top:50px;}
	.tg  {
		border-collapse:collapse;
		border-spacing:0;
		margin-bottom: 10px;
	}
	.tg1  {
		width: 400px;
		border-collapse:collapse;
		border-spacing:0;
		float: left;
		display: inline-table;
		margin-bottom:5px;
	}
	.tg td{font-family:Arial, sans-serif;font-size:10px;padding:5px 4px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;border-collapse:collapse;}
	.tg th{font-family:Arial, sans-serif;font-size:10px;font-weight:700;border-style:solid;padding:5px 4px;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg .tg-yw4l{vertical-align:middle}
	.tg1 td{font-family:Arial, sans-serif;font-size:8px;padding:5px 2px;border-style:solid;border-width:0;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 th{font-family:Arial, sans-serif;font-size:8px;font-weight:700;padding:5px 2px;border-style:solid;border-width:0;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 .tg-yw4l{vertical-align:middle}
	.tfoot {font-weight:700}
	.tc {text-align:center}
	.tl {text-align:left}
	.tr {text-align:right}
	.tg .tg-yw4l.tt {vertical-align:top}
	h4 {font-size:12px;padding-top:0;margin-top:0;margin-bottom:5px;}
	.subject_footer {
		font-family:Arial, sans-serif;
		position: relative;
		left: 0;
		bottom: 115px;
		font-size: 12px;
		font-weight: 500;
	}
	.fullwidth-image {
		width: 100px;
	}
	.subject_footer div {
		font-family:Arial, sans-serif;
		width: 120px;
		float: left;
		margin: 0 60px;
		font-weight: 500;
	}
	.subject_footer .name {
		font-family:Arial, sans-serif;
		margin-top: 50px;
		font-weight: 500;
	}
</style>

<h4 class="tc">LAPORAN BARANG DITERIMA <br/>&nbsp;</h4>


<table width="100%" class="tg">
	<tr>
		<th width="2%" class="tg-yw4l tc">NO.</th>
		<th width="10%" class="tg-yw4l tc">NAMA PRODUK</th>
		<th width="10%" class="tg-yw4l tc">KODE PRODUK</th>
		<th width="10%" class="tg-yw4l tc">QUANTITY</th>
		<th width="10%" class="tg-yw4l tc">RECEIPT NUMBER</th>
		<th width="10%" class="tg-yw4l tc">TANGGAL</th>
		<th width="10%" class="tg-yw4l tc">VENDOR</th>
		<th width="10%" class="tg-yw4l tc">WAREHOUSE</th>
		<th width="10%" class="tg-yw4l tc">NAMA PENGIRIM</th>
		<th width="10%" class="tg-yw4l tc">NAMA PENERIMA</th>
	</tr>
	
	<?php $no = 1; ?>
	<?php foreach($receiving_list as $receiving){ ?>
		<tr>
			<td class="tg-yw4l tc"><?php echo $no; ?></td>
			<td class="tg-yw4l tc"><?php echo $receiving->product_name; ?></td>
			<td class="tg-yw4l tc"><?php echo $receiving->product_code; ?></td>
			<td class="tg-yw4l tc"><?php echo $receiving->quantity; ?></td>
			<td class="tg-yw4l tc"><?php echo $receiving->receiving_number; ?></td>
			<td class="tg-yw4l tc"><?php echo tgl_indo($receiving->created_at); ?></td>
			<td class="tg-yw4l tc"><?php echo get_vendor_name($receiving->vendor_id); ?></td>
			<td class="tg-yw4l tc"><?php echo get_warehouse_name($receiving->warehouse_id); ?></td>
			<td class="tg-yw4l tc"><?php echo $receiving->sender_name; ?></td>
			<td class="tg-yw4l tc"><?php echo get_user_name($receiving->user_id); ?></td>
		</tr>
		<?php $no++; ?>
	<?php } ?>			

</table>

<div style="clear:both;"></div>

<script>
	window.print();
</script>