<style type="text/css">
	html { margin: 0}
	.tg  {
		width: 100%;
		border-collapse:collapse;
		border-spacing:0;
		margin-bottom: 10px;
	}
	.tg1  {
		width: 400px;
		border-collapse:collapse;
		border-spacing:0;
		float: left;
		display: inline-table;
		margin-bottom:5px;
	}
	.tg td{font-family:Arial, sans-serif;font-size:7px;padding:1px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg th{font-family:Arial, sans-serif;font-size:7px;font-weight:700;border-style:solid;padding:1px 3px;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
	.tg .tg-yw4l{vertical-align:middle}
	.tg1 td{font-family:Arial, sans-serif;font-size:7px;padding:1px 3px;border-style:solid;border-width:0;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 th{font-family:Arial, sans-serif;font-size:7px;font-weight:700;padding:1px 3px;border-style:solid;border-width:0;overflow:hidden;word-break:normal;border-color:black;}
	.tg1 .tg-yw4l{vertical-align:middle}
	.tfoot {font-weight:700}
	.tc {text-align:center}
	.tl {text-align:left}
	.tr {text-align:right}
	.tg .tg-yw4l.tt {vertical-align:top}
	h4 {font-size:12px;padding-top:0;margin-top:0;margin-bottom:5px;}
	.subject_footer {
		font-family:Arial, sans-serif;
		position: relative;
		left: 0;
		bottom: 115px;
		font-size: 12px;
		font-weight: 500;
	}
	.fullwidth-image {
		width: 100px;
	}
	.subject_footer div {
		font-family:Arial, sans-serif;
		width: 120px;
		float: left;
		margin: 0 60px;
		font-weight: 500;
	}
	.subject_footer .name {
		font-family:Arial, sans-serif;
		margin-top: 50px;
		font-weight: 500;
	}
</style>

<div class="content-body">

	<!-- `new` constructor table -->
	<section id="constructor">
		<div class="row">
			<div class="col-12">
			<div class="card">
				<div class="card-header">
					<form class="form form-horizontal mt-2" method="POST" target="_blank" action="<?php echo base_url(); ?>laporan/barang_diterima" >
						<div class="form-group row">
							<label class="col-md-4 pb-1 label-control" for="date_in">Dari Tanggal</label>
							<div class="col-md-4">
								<input type="date" id="start_date" value="<?php echo (!empty($start_date) ? $start_date : ''); ?>" class="form-control" name="start_date" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Dari Tanggal" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-4 pb-1 label-control" for="date_in">Sampai Tanggal</label>
							<div class="col-md-4">
								<input type="date" id="end_date" value="<?php echo (!empty($end_date) ? $end_date : ''); ?>"  class="form-control" name="end_date" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Sampai Tanggal" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-4 pb-1 label-control" for="date_in"></label>
							<div class="col-md-4">
								<button value="flag" name="flag" type="submit" class="btn-md btn btn-primary">
									<i class="la la-print"></i> Print
								</button>
							</div>
						</div>
						
					</form>
				</div>
				<div class="card-content collapse show">
					<div class="card-body card-dashboard">
						
						

					</div>
				</div>
			</div>
		</div>
	</section>


  	<?php if (!empty($flag)): ?>

		<h4 class="tc">SUMMARY REPORT MONTHLY <br/><?php echo get_month_name_long($report_month). ' ' . get_year_from_report_month($report_month); ?></h4>

		<div style="clear:both;"></div>

	
<?php endif; ?>

</div>