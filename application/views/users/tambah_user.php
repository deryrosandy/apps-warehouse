<div class="content-header row">
  <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">Data User</h3>
    <div class="row breadcrumbs-top d-inline-block">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a>
          </li>
          <li class="breadcrumb-item"><a href="#">Tambah User</a>
          </li>
        </ol>
      </div>
    </div>
  </div>
  <!--
  <div class="content-header-right col-md-6 col-12">
    <div class="dropdown float-md-right ">
      <a href="<?php //echo base_url(); ?>barang/tambah" class="btn btn-danger btn-glow px-2 round text-bold-500 white"><i class="ft-plus white"></i>Barang</a>
    </div>
  </div>
  -->
</div>

<div class="content-body">

  <section id="row-separator-form-layouts">
    <div class="row">
      
      <div class="col-md-9">

        <div class="card">
          <div class="card-header">
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>

          <div class="card-content collapse show">
            <div class="card-body">
              <form class="form form-horizontal row-separator" method="POST" action="<?php echo base_url(); ?>users/insert_user" >
                <div class="form-body">
                  <h4 class="form-section"><i class="la la-clipboard"></i> Tambah User</h4>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_code">Username</label>
                    <div class="col-md-9">
                      <input type="text" id="product_code" value="<?php echo (isset($user) ? $user->username : ''); ?>" class="form-control" placeholder="Masukkan Username" name="username"  data-title="Username" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_code">Nama Depan</label>
                    <div class="col-md-9">
                      <input type="text" id="" value="<?php echo (isset($user) ? $user->first_name : ''); ?>" class="form-control" placeholder="Masukkan Nama Depan" name="first_name"  data-title="Nama Depan" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_code">Nama Belakang</label>
                    <div class="col-md-9">
                      <input type="text" id="" value="<?php echo (isset($user) ? $user->last_name : ''); ?>" class="form-control" placeholder="Masukkan Nama Belakang" name="last_name"  data-title="Nama Belakang" data-toggle="tooltip" data-trigger="hover" data-placement="top">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_code">Email</label>
                    <div class="col-md-9">
                      <input type="email" id="" value="<?php echo (isset($user) ? $user->email : ''); ?>" class="form-control" placeholder="Masukkan Emaail" name="email"  data-title="Email" data-toggle="tooltip" data-trigger="hover" data-placement="top">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_code">Password</label>
                    <div class="col-md-9">
                      <input type="password" class="form-control" placeholder="<?php echo (isset($user) ? 'Biarkan Kosong Jika Password Tidak Di Ganti': 'Password'); ?>" name="password"  data-title="Password" data-toggle="tooltip" data-trigger="hover" data-placement="top">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_code">No. Telp</label>
                    <div class="col-md-9">
                      <input type="text" id="" value="<?php echo (isset($user) ? $user->phone_number : ''); ?>" class="form-control" placeholder="Masukkan No. Telp" name="phone_number"  data-title="Password" data-toggle="tooltip" data-trigger="hover" data-placement="top">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_category_id">Type User</label>
                    <div class="col-md-9">
                      <select id="user_type" name="user_type" class="select2 form-control">
                        <option value="0">- Pilih Type User -</option>
                        <option value="administrator" <?php echo (isset($user) ? ($user->user_type=='administrator' ? 'selected':'') : ''); ?>>Administrator</option>
                        <option value="staff" <?php echo (isset($user) ? ($user->user_type=='staff' ? 'selected':'') : ''); ?>>Staff</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row skin skin-square">
                    <label class="col-md-3 label-control" for="active">Status</label>
                    <div class="col-md-9">
                      <fieldset>
                        <input type="checkbox" name="active" id="active" checked>
                      </fieldset>
                    </div>
                  </div>
                  <input type="hidden"  value="<?php echo (isset($user) ? $user->user_id : ''); ?>" id="user_id" class="form-control" name="user_id">
                </div>

                <div class="pt-2 pb-2 pull-right">
                  <button type="button" class="btn btn-warning mr-1" onclick="history.back(-1)" >
                    <i class="la la-remove"></i> Cancel
                  </button>
                  <button value="flag" name="flag" type="submit" class="btn btn-primary">
                    <i class="la la-check"></i> Submit
                  </button>
                </div>
                 
              </form>
            </div>
          </div>

        </div>
      </div>
      <!--
      <div class="col-md-3">
        <div class="card alert bg-warning ">
            <div class="card-header">
              <h4 class="card-title">Project Overview</h4>
              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-content">
              <div class="card-body">
                <p>
                  <strong>Pellentesque habitant morbi tristique</strong> senectus et netus
                  et malesuada fames ac turpis egestas. Vestibulum tortor quam,
                  feugiat vitae.
                  <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend
                  leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum
                  erat wisi, condimentum sed, <code>commodo vitae</code>, ornare
                  sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum,
                  eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.
                  <a href="#">Donec non enim</a>.</p>
              </div>
            </div>
          </div>
      </div>
      -->

    </div>
  </section>

</div>