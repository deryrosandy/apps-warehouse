<div class="msg" style="">
  <?php echo $this->session->flashdata('msg'); ?>
</div>

<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-20">
            
      <div class="row">
          
        <div class="col-xl-12 mg-t-0 mg-b-0">
          <div class="form-layout form-layout-5  border-0 pd-0">
            <div class="d-flex align-items-center justify-content-between">
              <h4 class="tx-inverse tx-normal tx-roboto mg-b-20">Data Supplier</h4>
              <button class="btn btn-info btn-sm tx-roboto tx-normal"><i class="fa fa-plus"></i> Add Supplier</button>
            </div>
            <div class="table-wrapper">
              <table id="datatables_therapist" class="table display responsive nowrap">
                <thead>
                  <tr>
                    <th class="wd-5p">No.</th>
                    <th class="wd-5p">Name</th>
                    <th class="wd-15p">Phone Number</th>
                    <th class="wd-20p">Description</th>
                    <th class="wd-25p">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1; ?>
                  <?php foreach ($suppliers as $supplier): ?>
                    <tr>
                    <td><?php echo $no; ?></td>
                      <td><?php echo $supplier->name; ?></td>
                      <td><?php echo $supplier->phone_number; ?></td>
											<td><?php echo $supplier->description; ?></td>
                      <td>
                        <button data-toggle="modal" data-target="#modal_supplier" data-id="<?php echo $supplier->supplier_id; ?>" data-toggle="tooltip-danger" data-placement="top"  title="Edit Supplier" class="btn btn-info btn-sm">Edit</button>
                      </td>
                    </tr>
                    <?php $no++; ?>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- form-layout -->
        </div>
      
      </div>

  </div>
</div>