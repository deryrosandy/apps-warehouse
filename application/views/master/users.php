<div class="msg" style="">
  <?php echo $this->session->flashdata('msg'); ?>
</div>

<div class="br-pagebody">
  
  <div class="br-section-wrapper pd-20">
            
      <div class="row">
          
        <div class="col-xl-12 mg-t-0 mg-b-0">
          <div class="form-layout form-layout-5  border-0 pd-0">
            <div class="d-flex align-items-center justify-content-between">
              <h4 class="tx-inverse tx-normal tx-roboto mg-b-20">Data User</h4>
              <button class="btn btn-info btn-sm tx-roboto tx-normal"><i class="fa fa-plus"></i> Add User</button>
            </div>
            <div class="table-wrapper">
              <table id="datatables_user" class="table display responsive nowrap">
                <thead>
                  <tr>
                    <th class="wd-5p">No.</th>
                    <th class="wd-5p">Username</th>
                    <th class="wd-15p">Full Name</th>
                    <th class="wd-20p">Email</th>
                    <th class="wd-15p">Phone Number</th>
                    <th class="wd-10p">Branch</th>
                    <th class="wd-10p">User Type</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-25p">Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $no = 1; ?>
                  <?php foreach ($users as $user): ?>
                    <tr>
                    <td><?php echo $no; ?></td>
                      <td><?php echo $user->username; ?></td>
                      <td><?php echo $user->first_name; ?> <?php echo $user->last_name; ?></td>
											<td><?php echo $user->email; ?></td>
											<td><?php echo $user->phone_number; ?></td>
                      <td><?php echo get_branch_name($user->branch_id); ?></td>                      
                      <td><?php echo get_level_user($user->user_type); ?></td>
                      <td><?php echo get_status_active($user->active); ?></td>
                      <td>
                        <button data-toggle="modal" data-target="#modal_user" data-id="<?php echo $user->user_id; ?>" data-toggle="tooltip-danger" data-placement="top"  title="Edit User" class="btn btn-info btn-sm btn-user">Edit </button>
                      </td>
                    </tr>
                    <?php $no++; ?>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- form-layout -->
        </div>
      
      </div>

  </div>
</div>

<div id="modal_user" class="modal fade" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bd-0">
    <div class="modal-header pd-y-20 pd-x-25">
      <h4 class="mg-b-5 tx-inverse lh-2 tx-uppercase">Edit User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      
      <div class="modal-body pd-0">
        <div class="row no-gutters">          
          <div class="col-lg-12 bg-white">
            <div class="pd-30">
              
            <div class="form-layout border-0 pd-y-0">
              
                <h4 class="tx-inverse tx-normal tx-roboto mg-b-20 fullname"></h4>
                <input type="hidden" name="therapist_id" class="kodekar" />

                <div class="form-layout form-layout-1">
                  <div class="row mg-b-25">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label">Username: </label>
                        <input class="form-control" type="text" name="username" value="" placeholder="Enter Username">
                      </div>
                    </div><!-- col-4 -->
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label">Firstname: </label>
                        <input class="form-control" type="text" name="firs_tname" value="" placeholder="Enter firstname">
                      </div>
                    </div><!-- col-4 -->
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label">Lastname: </label>
                        <input class="form-control" type="text" name="last_name" value="McDoe" placeholder="Enter lastname">
                      </div>
                    </div><!-- col-4 -->
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label">Email address: </label>
                        <input class="form-control" type="text" name="email" value="" placeholder="Enter email address">
                      </div>
                    </div><!-- col-4 -->
                    <div class="col-lg-6">
                      <div class="form-group mg-b-10-force">
                        <label class="form-control-label">Phone Number: </label>
                        <input class="form-control" type="text" name="phone_number" value="" placeholder="Enter Phone Number">
                      </div>
                    </div><!-- col-8 -->
                    <div class="col-lg-4">
                      <div class="form-group mg-b-10-force">
                        <label class="form-control-label">User Type: </label>
                        <select class="form-control select" data-placeholder="User Type">
                          <option label="- Choose User type -"></option>
                          <option value="admin">Administrator</option>
                          <option value="manager">Manager</option>
                          <option value="supervisor">Supervisor</option>
                        </select>
                      </div>
                    </div><!-- col-4 -->
                    <div class="col-lg-4">
                      <div class="form-group mg-b-10-force">
                        <label class="form-control-label">Branch: </label>
                        <select class="form-control select" data-placeholder="User Type">
                          <option label="- Choose User type -"></option>
                          <option value="admin">Administrator</option>
                          <option value="manager">Manager</option>
                          <option value="supervisor">Supervisor</option>
                        </select>
                      </div>
                    </div><!-- col-4 -->
                  </div><!-- row -->

                <div class="form-layout-footer">
                  <button class="btn btn-info">Submit Form</button>
                  <button class="btn btn-secondary">Cancel</button>
                </div><!-- form-layout-footer -->
              </div><!-- form-layout -->

            </div><!-- pd-20 -->

          </div><!-- col-6 -->
        </div><!-- row -->
      </div><!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div>