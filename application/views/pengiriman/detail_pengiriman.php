<div class="content-header row">
  <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">Penerimaan</h3>
    <div class="row breadcrumbs-top d-inline-block">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a>
          </li>
          <li class="breadcrumb-item"><a href="#">Approval Penerimaan</a>
          </li>
        </ol>
      </div>
    </div>
  </div>
  <!--
  <div class="content-header-right col-md-6 col-12">
    <div class="dropdown float-md-right ">
      <a href="<?php //echo base_url(); ?>barang/tambah" class="btn btn-danger btn-glow px-2 round text-bold-500 white"><i class="ft-plus white"></i>Barang</a>
    </div>
  </div>
  -->
</div>

<div class="content-body">

  <section id="row-separator-form-layouts">
    <div class="row">
      
      <div class="col-md-10">
     
      <div class="msg" style="">
        <?php echo $this->session->flashdata('msg'); ?>
      </div>
      
        <div class="card">
          <div class="card-content collapse show">
            <div class="card-body">

              <form class="form form-horizontal" method="POST" action="<?php echo base_url(); ?>pengiriman/approval_pengiriman/<?php echo $shipment->shipment_id; ?>" >
                <div class="form-body">
                  <h4 class="form-section mb-3"><i class="la la-clipboard"></i> Approval Pengiriman</h4>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="shipment_number">No. Pengiriman</label>
                    <div class="col-md-9">
                    <h5>: <?php echo $shipment->shipment_number; ?></h5>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="date_out">Tanggal</label>
                    <div class="col-md-9">
                      <h5>: <?php echo tgl_indo($shipment->date_out); ?></h5>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="customer_id">Customer</label>
                    <div class="col-md-9">
                    <h5>: <?php echo get_customer_name($shipment->customer_id); ?></h5>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="vendor_id">From Warehouse</label>
                    <div class="col-md-9">
                    <h5>: <?php echo get_warehouse_name($shipment->warehouse_id); ?></h5>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="sender_name">Nama Pengirim</label>
                    <div class="col-md-9">
                      <h5>: <?php echo $shipment->sender_name; ?></h5>
                    </div>
                  </div>
                </div>

                <div class="form-body">
                  <h4 class="form-section"><i class="la la-clipboard"></i> List Barang</h4>
                            
                    <div class="form-group row contact-repeater">
                      <div class="col-md-12" data-repeater-list="product_list">

                      <div id="items-list">
                        <table class="table table-bordered table-condensed table-striped items-table">
                          <thead>
                            <tr>
                              <th>No.</th>
                              <th>Kode Barang</th>
                              <th>Nama Barang</th>
                              <th>Qty</th>
                            </tr>
                          </thead>
                          <tbody>

                          <?php $no = 1; ?>
                          <?php foreach($product_list as $product){ ?>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $product->product_code; ?></td>
                              <td><?php echo $product->product_name; ?></td>
                              <td><?php echo $product->quantity; ?></td>
                            </tr>
                            <?php $no++; ?>
                          <?php } ?>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  
                  <input type="hidden" id="shipment_id"  value="<?php echo $shipment->shipment_id; ?>" name="shipment_id">

                </div>

                  <div class="pt-0 pb-3 pull-right">
                    <button type="button" class="btn btn-warning mr-1">
                      <i class="la la-remove"></i> Cancel
                    </button>
                    <button title="Set To Process" type="submit" value="process" class="btn btn-teal mr-1" name="set_process">
                    <i class="la la-truck"></i> Set To Process
                    </button>
                    <button title="Set To Completed" name="set_completed"  value="done"   type="submit" class="btn btn-success">
                    <i class="la la-check-circle"></i> Set To Completed
                    </button>
                  </div>
                 
              </form>
            </div>
          </div>

        </div>
      </div>
      
      <!--
      <div class="col-md-3">
        <div class="card alert bg-warning ">
            <div class="card-header">
              <h4 class="card-title">Project Overview</h4>
              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-content">
              <div class="card-body">
                <p>
                  <strong>Pellentesque habitant morbi tristique</strong> senectus et netus
                  et malesuada fames ac turpis egestas. Vestibulum tortor quam,
                  feugiat vitae.
                  <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend
                  leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum
                  erat wisi, condimentum sed, <code>commodo vitae</code>, ornare
                  sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum,
                  eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.
                  <a href="#">Donec non enim</a>.</p>
                <p>
                  <strong>Lorem ipsum dolor sit</strong>
                </p>
                <ol>
                  <li>Consectetuer adipiscing</li>
                  <li>Aliquam tincidunt mauris</li>
                  <li>Consectetur adipiscing</li>
                  <li>Vivamus pretium ornare</li>
                  <li>Curabitur massa</li>
                </ol>
              </div>
            </div>
          </div>
      </div>
      -->
    </div>
  </section>

</div>