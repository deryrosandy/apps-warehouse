<div class="content-header row">
  <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">Penerimaan</h3>
    <div class="row breadcrumbs-top d-inline-block">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a>
          </li>
          <li class="breadcrumb-item"><a href="#"><?php echo (isset($shipment) ? 'Edit' : 'Tambah'); ?> Penerimaan</a>
          </li>
        </ol>
      </div>
    </div>
  </div>
  <!--
  <div class="content-header-right col-md-6 col-12">
    <div class="dropdown float-md-right ">
      <a href="<?php //echo base_url(); ?>barang/tambah" class="btn btn-danger btn-glow px-2 round text-bold-500 white"><i class="ft-plus white"></i>Barang</a>
    </div>
  </div>
  -->
</div>

<div class="content-body">

  <section id="row-separator-form-layouts">
    <div class="row">
      
      <div class="col-md-10">

        <div class="card">
          <div class="card-header">
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>

          <div class="card-content collapse show">
            <div class="card-body">
              <form class="form form-horizontal row-separator" method="POST" action="<?php echo base_url(); ?>pengiriman/insert_pengiriman" >
                <div class="form-body">
                  <h4 class="form-section"><i class="la la-clipboard"></i> <?php echo (isset($shipment) ? 'Edit' : 'Tambah'); ?> Pengiriman</h4>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="shipment_number">No. DO</label>
                    <div class="col-md-9">
                      <input type="text" id="shipment_number" value="<?php echo (isset($shipment) ? $shipment->shipment_number : $no_do); ?>"  readonly class="form-control" placeholder="Masukkan No. DO" name="shipment_number"  data-title="No. DO" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="date_out">Tanggal</label>
                    <div class="col-md-9">
                      <input type="date" id="date_out" value="<?php echo (isset($shipment) ? date("Y-m-d", strtotime($shipment->date_out)) : ''); ?>" class="form-control" name="date_out" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Tanggal" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="customer_id">Customer</label>
                    <div class="col-md-9">
                      <select id="customer_id" name="customer_id" class="select2 form-control">
                        <option value="0">- Pilih Customer -</option>
                        <?php foreach($customers as $customer){ ?>
                            <option  value="<?php echo $customer->customer_id; ?>" <?php echo (isset($shipment) ? ($shipment->customer_id == $customer->customer_id ? 'selected' : '') : ""); ?>><?php echo $customer->customer_name; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="vendor_id">From Warehouse</label>
                    <div class="col-md-9">
                      <select id="warehouse_id" name="warehouse_id" class="select2 form-control" data-title="Warehouse" data-toggle="tooltip" data-trigger="hover" data-placement="top" >
                        <option value="0">- Pilih Warehouse -</option>
                        <?php foreach($warehouse as $ware){ ?>
                            <option  value="<?php echo $ware->warehouse_id; ?>" <?php echo (isset($shipment) ? ($shipment->warehouse_id == $ware->warehouse_id ? 'selected' : '') : ""); ?> ><?php echo $ware->warehouse_name; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="sender_name">Nama Pengirim</label>
                    <div class="col-md-9">
                      <input type="text" id="sender_name"  value="<?php echo (isset($shipment) ? $shipment->sender_name : ''); ?>"  class="form-control" placeholder="Masukkan Nama Pengirim" name="sender_name"  data-title="Nama Pengirim" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                    </div>
                  </div>
                </div>

                <div class="form-body">
                  <h4 class="form-section"><i class="la la-clipboard"></i> List Barang</h4>
                  
                  <div class="form-group row contact-repeater">
                    <div class="col-md-12" data-repeater-list="product_list">

                     <?php if(isset($product_list)){ ?>

                    <?php foreach($product_list as $list){ ?>
                      <div class="row input-group mb-1" data-repeater-item>
                          <div class="col-md-8">
                            <select id="" name="product_id" class="form-control" data-title="Tambah Barang" data-toggle="tooltip" data-trigger="hover" data-placement="top" >
                              <option value="0">- Pilih Barang -</option>
                              <?php foreach($products as $product){ ?>
                                  <option  value="<?php echo $product->product_id; ?>" <?php echo ($list->product_id == $product->product_id ? 'selected' : ''); ?>><?php echo $product->product_name; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-md-2">
                            <input type="text" class="form-control" value="<?php echo (isset($list) ? $list->quantity : ''); ?>" placeholder="Qty" name="quantity"  data-title="Qty" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                          </div>
                          <div class="col-md-2">
                            <span class="input-group-append" id="button-addon2">
                              <button class="btn btn-danger" type="button" data-repeater-delete><i class="ft-x"></i></button>
                            </span>
                          </div>
                      </div>
                    <?php } ?>
                    
                    <?php }else{ ?>
                      
                      <div class="row input-group mb-1" data-repeater-item>
                          <div class="col-md-8">
                            <select id="" name="product_id" class="form-control" data-title="Tambah Barang" data-toggle="tooltip" data-trigger="hover" data-placement="top" >
                              <option value="0">- Pilih Barang -</option>
                              <?php foreach($products as $product){ ?>
                                  <option value="<?php echo $product->product_id; ?>" ><?php echo $product->product_name; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Qty" name="quantity"  data-title="Qty" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                          </div>
                          <div class="col-md-2">
                            <span class="input-group-append" id="button-addon2">
                              <button class="btn btn-danger" type="button" data-repeater-delete><i class="ft-x"></i></button>
                            </span>
                          </div>
                      </div>

                    <?php } ?>

                    </div>
                    <div class="col-md-12 pt-0">
                      <button type="button" data-repeater-create class="btn btn-info">
                        <i class="ft-plus"></i> Tambah
                      </button>
                    </div>
                  </div>
                  
                  <input type="hidden" id="shipment_id"  value="<?php echo (isset($shipment) ? $shipment->shipment_id : ''); ?>" name="shipment_id">

                </div>

                  <div class="pt-2 pb-2 pull-right">
                    <button type="button" onclick="history.back(-1)" class="btn btn-warning mr-1">
                      <i class="la la-remove"></i> Cancel
                    </button>
                    <button value="flag" name="flag" type="submit" class="btn btn-primary">
                      <i class="la la-check"></i> Submit
                    </button>
                  </div>
                 
              </form>
            </div>
          </div>

        </div>
      </div>
      
      <!--
      <div class="col-md-3">
        <div class="card alert bg-warning ">
            <div class="card-header">
              <h4 class="card-title">Project Overview</h4>
              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-content">
              <div class="card-body">
                <p>
                  <strong>Pellentesque habitant morbi tristique</strong> senectus et netus
                  et malesuada fames ac turpis egestas. Vestibulum tortor quam,
                  feugiat vitae.
                  <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend
                  leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum
                  erat wisi, condimentum sed, <code>commodo vitae</code>, ornare
                  sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum,
                  eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.
                  <a href="#">Donec non enim</a>.</p>
                <p>
                  <strong>Lorem ipsum dolor sit</strong>
                </p>
                <ol>
                  <li>Consectetuer adipiscing</li>
                  <li>Aliquam tincidunt mauris</li>
                  <li>Consectetur adipiscing</li>
                  <li>Vivamus pretium ornare</li>
                  <li>Curabitur massa</li>
                </ol>
              </div>
            </div>
          </div>
      </div>
      -->
    </div>
  </section>

</div>