<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>.:: Faktur Pengiriman Barang ::.</title>
		
        <style type="text/css">
		.edit-mode-wrap {
			padding: 0;
			margin:0;
		}
		.totals-row td {
			border-right:none !important;
			border-left:none !important;
		}
		
		
		td {
			white-space: nowrap;
		}
		.items-table td ,#notes { white-space:normal;}
		.totals-row td strong,.items-table th {
			white-space:nowrap;
		}
		</style>
				<style type="text/css">
			.is_logo {display:none;}
		</style>
	</head>
	<body>
		<div id="editor" class="edit-mode-wrap">
			<style type="text/css">
				</style><style type="text/css">
					* { 
						margin:0;
						padding:0; 
					}
					body { 
						background:#fff;
						font-family:Arial, Helvetica, sans-serif;
						font-size:14px;
						line-height:20px;
						padding:0;
						height: 100%;
					}
					#extra {text-align: right; font-size: 22px;  font-weight: 700}
					.invoice-wrap { width:100%; margin:0 auto; background:#FFF; color:#000;}
					.invoice-inner { margin:0; padding:20px }
					.invoice-address { border-top: 3px double #000000; margin: 15px 0; padding-top: 20px; padding-bottom:10px;}
					.bussines-name { font-size:18px; font-weight:100 }
					.invoice-name { font-size:22px; font-weight:700 }
					.listing-table th { background-color: #ffffff; border-bottom: 1px solid #555555; border-top: 1px solid #555555; font-weight: bold; text-align:left; padding:6px 4px }
					.listing-table td { border-bottom: 1px solid #555555; text-align:left; padding:5px 6px; vertical-align:top }
					.total-table td { border-left: 1px solid #555555; }
					.total-row { background-color: #ffffff; border-bottom: 1px solid #555555; border-top: 1px solid #555555; font-weight: bold; }
					.row-items { margin:0 0; display:block }
					.notes-block {
						margin:50px 0 0 0 }
					/*tables*/
					table td { vertical-align:top}
					.items-table { border:1px solid #1px solid #555555; border-collapse:collapse; width:100%}
					.items-table td, .items-table th { border:1px solid #555555; padding:4px 5px ; text-align:left}
					.items-table th { background:#f5f5f5;}
					.totals-row .wide-cell { border:1px solid #fff; border-right:1px solid #555555; border-top:1px solid #555555}
				</style>
				<div class="invoice-wrap">
					<div class="invoice-inner">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tbody>
								<tr>
									<td valign="top" align="right">
									<div class="business_info">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tbody>
											<tr>
												<td>
													<span class="editable-area" id="business_info">
														<p style="font-size: 12pt;"><span style="font-size: 20pt;">PT. Arwana Citramulia</span><br/>
														Jakarta Timur - DKI Jakarta<br/></p>
													</span>
												</td>
											</tr>
										</tbody>
									</table>
									</div>
									</td>
									<td valign="top" align="right">
										<p class="editable-text" id="extra"><span style="font-size: 18pt;">Faktur Penerimaan</span></p>
									</td>
								</tr>
							</tbody>
						</table>

						<div class="invoice-address">
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tbody>
									<tr>
										<td valign="top" align="left" width="50%">
											<table cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<td style="" valign="top" width=""><strong><span class="editable-text" id="label_bill_to">Vendor</span></strong></td>
														<td valign="top">
															<div class="client_info">
																<table cellspacing="0" cellpadding="0" border="0">
																	<tbody>
																		<tr>
																			<td style="padding-left:25px;"><span><?php echo get_vendor_name($receiving->vendor_id); ?><br>
																				</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td valign="top" align="right" width="50%">
											<table cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<td align="right"><strong><span class="editable-text" id="label_invoice_no">No. Faktur</span></strong></td>
														<td style="padding-left:20px" align="left"><span class="editable-text" id="no"><?php echo $receiving->receiving_number; ?></span></td>
													</tr>
													<tr>
														<td align="right"><strong><span class="editable-text" id="label_date">Tgl. Penerimaan</span></strong></td>
														<td style="padding-left:20px" align="left"><span class="editable-text" id="date"><?php echo tgl_indo($receiving->date_in); ?></span></td>
													</tr>
													<tr>
														<td align="right"><strong><span class="editable-text" id="label_date">Jam</span></strong></td>
														<td style="padding-left:20px" align="left"><span class="editable-text" id="date"><?php echo getHour($receiving->created_at); ?></span></td>
													</tr>
													<tr>
														<td align="right"><strong><span class="editable-text" id="label_date">Nama Pengirim</span></strong></td>
														<td style="padding-left:20px" align="left"><span class="editable-text" id="date"><?php echo $receiving->sender_name; ?></span></span></td>
													</tr>
													<tr>
														<td align="right"><strong><span class="editable-text" id="label_date">Warehouse</span></strong></td>
														<td style="padding-left:20px" align="left"><span class="editable-text" id="date"><?php echo get_warehouse_name($receiving->warehouse_id); ?></span></span></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="items-list">
							<table class="table table-bordered table-condensed table-striped items-table">
								<thead>
									<tr>
										<th>No.</th>
										<th>Kode Barang</th>
										<th>Nama Barang</th>
										<th>Qty</th>
									</tr>
								</thead>
								<tbody>

								<?php $no = 1; ?>
								<?php foreach($product_list as $product){ ?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $product->product_code; ?></td>
										<td><?php echo $product->product_name; ?></td>
										<td><?php echo $product->quantity; ?></td>
									</tr>
									<?php $no++; ?>
								<?php } ?>

								</tbody>
							</table>
						</div>
						<!--
						<div class="notes-block">
							<div class="editable-area" id="notes" style=""><b>Perhatian:</b></div>
							<div class="notice">1. Pengambilan barang harap di sertai nota</div>
						</div>
						-->
					</div>
				</div>
			</div>
		<style>
body {
    background: #ffffff;
	padding:0;
}
.invoice-wrap {
	box-shadow: 0 0 4px rgba(0, 0, 0, 0.1);
}
#mobile-preview-close a {
	position:fixed; 
	left:20px;
	bottom:30px; 
	background-color: #fff;
	font-weight: 600;
	outline: 0 !important;
	line-height: 1.5;
	border-radius: 3px;
	font-size: 14px;
	padding: 7px 10px;
	border:1px solid #fff;
	text-decoration:none;
}
#mobile-preview-close img {
	width:20px;
	height:auto;
}
#mobile-preview-close a:nth-child(2) {
left:190px;
background:#f5f5f5;
border:1px solid #9f9f9f;
color:#555 !important;
}
#mobile-preview-close a:nth-child(2) img {
    height: auto;
	position: relative;
	top: 2px;
}
.invoice-wrap {padding: 20px;}


@media print {
  #mobile-preview-close a {
  display:none
}
body {
    background: none;
}
.invoice-wrap {box-shadow: none; margin-bottom: 0px;}

}
</style>

</body>

</html>