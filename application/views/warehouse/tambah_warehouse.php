<div class="content-header row">
  <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">Data warehouse</h3>
    <div class="row breadcrumbs-top d-inline-block">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a>
          </li>
          <li class="breadcrumb-item"><a href="#">Tambah Warehouse</a>
          </li>
        </ol>
      </div>
    </div>
  </div>
  <!--
  <div class="content-header-right col-md-6 col-12">
    <div class="dropdown float-md-right ">
      <a href="<?php //echo base_url(); ?>barang/tambah" class="btn btn-danger btn-glow px-2 round text-bold-500 white"><i class="ft-plus white"></i>Barang</a>
    </div>
  </div>
  -->
</div>

<div class="content-body">

  <section id="row-separator-form-layouts">
    <div class="row">
      
      <div class="col-md-9">

        <div class="card">
          <div class="card-header">
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>

          <div class="card-content collapse show">
            <div class="card-body">
              <form class="form form-horizontal row-separator" method="POST" action="<?php echo base_url(); ?>warehouse/insert_warehouse" >
                <div class="form-body">
                  <h4 class="form-section"><i class="la la-clipboard"></i> Tambah warehouse</h4>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="product_code">Nama warehouse</label>
                    <div class="col-md-9">
                      <input type="text" id="product_code" value="<?php echo (isset($warehouse) ? $warehouse->warehouse_name : ''); ?>" class="form-control" placeholder="Masukkan Nama warehouse" name="warehouse_name"  data-title="Nama warehouse" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 label-control" for="receiving_number">Keterangan</label>
                    <div class="col-md-9">
                    <textarea rows="5" class="form-control" name="description" placeholder="Keterangan"  data-title="Keterangan" data-toggle="tooltip" data-trigger="hover" data-placement="top" ><?php echo (isset($warehouse) ? $warehouse->description : ''); ?></textarea>
                    </div>
                  </div>
                  <input type="hidden"  value="<?php echo (isset($warehouse) ? $warehouse->warehouse_id : ''); ?>" id="warehouse_id" class="form-control" name="warehouse_id">
                </div>

                <div class="pt-2 pb-2 pull-right">
                  <button type="button" class="btn btn-warning mr-1" onclick="history.back(-1)" >
                    <i class="la la-remove"></i> Cancel
                  </button>
                  <button value="flag" name="flag" type="submit" class="btn btn-primary">
                    <i class="la la-check"></i> Submit
                  </button>
                </div>
                 
              </form>
            </div>
          </div>

        </div>
      </div>
      <!--
      <div class="col-md-3">
        <div class="card alert bg-warning ">
            <div class="card-header">
              <h4 class="card-title">Project Overview</h4>
              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-content">
              <div class="card-body">
                <p>
                  <strong>Pellentesque habitant morbi tristique</strong> senectus et netus
                  et malesuada fames ac turpis egestas. Vestibulum tortor quam,
                  feugiat vitae.
                  <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend
                  leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum
                  erat wisi, condimentum sed, <code>commodo vitae</code>, ornare
                  sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum,
                  eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.
                  <a href="#">Donec non enim</a>.</p>
              </div>
            </div>
          </div>
      </div>
      -->

    </div>
  </section>

</div>