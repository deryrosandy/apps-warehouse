<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">

  <?php if($this->userdata->user_type=='administrator'): ?>

    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class="<?php echo ($page == 'dashboard' ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('dashboard'); ?>"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a> </li>
      <li class="<?php echo (($page == 'tambah_pengiriman') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('pengiriman/tambah_pengiriman'); ?>"><i class="la la-pencil-square"></i><span class="menu-title" data-i18n="nav.dash.main">Tambah Pengiriman</span></a> </li>
      <li class="<?php echo (($page == 'tambah_penerimaan') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('penerimaan/tambah_penerimaan'); ?>"><i class="la la-pencil"></i><span class="menu-title" data-i18n="nav.dash.main">Tambah Penerimaan</span></a> </li>
      <li class="<?php echo (($page == 'data_pengiriman' || $page == 'data_open'|| $page == 'data_proses' || $page == 'data_selesai') ? 'active' : ''); ?> nav-item"><a href="#"><i class="la la-truck"></i><span class="menu-title" data-i18n="nav.users.main">Data Pengiriman</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'data_open') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('pengiriman/data_open'); ?>" data-i18n="nav.users.user_profile">Open</a>
          </li>
          <li class="<?php echo (($page == 'data_proses') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('pengiriman/data_proses'); ?>" data-i18n="nav.users.users_contacts">Proses</a>
          </li>
          <li class="<?php echo (($page == 'data_selesai') ? 'active' : ''); ?>">
            <a class=" menu-item" href="<?php echo base_url('pengiriman/data_selesai'); ?>" data-i18n="nav.users.users_contacts">Selesai</a>
          </li>
        </ul>
      </li>
      <li class="<?php echo (($page == 'data_penerimaan') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('penerimaan'); ?>"><i class="la la-shopping-cart"></i><span class="menu-title" data-i18n="nav.dash.main">Data Penerimaan</span></a> </li>
      <li class="<?php echo (($page == 'approval_pengiriman') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('pengiriman/approval'); ?>"><i class="la la-check-circle"></i><span class="menu-title" data-i18n="nav.dash.main">Approval Pengiriman</span></a> </li>
      <li class="<?php echo (($page == 'stock_barang') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('barang/stock_barang'); ?>"><i class="la la-cube"></i><span class="menu-title" data-i18n="nav.dash.main">Stock Barang</span></a> </li>
      
      <li class=" nav-item"><a href="#"><i class="la la-print"></i><span class="menu-title" data-i18n="nav.users.main">Laporan</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'laporan_barang_diterima') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('laporan/barang_diterima'); ?>" data-i18n="nav.users.user_profile">Barang Di Terima</a>
          </li>
          <li class="<?php echo (($page == 'laporan_barang_dikirim') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('laporan/barang_dikirim'); ?>" data-i18n="nav.users.users_contacts">Barang Di Kirim</a>
          </li>
          <li class="<?php echo (($page == 'laporan_stock_barang') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('laporan/stock_barang'); ?>" data-i18n="nav.users.users_contacts">Stock Barang</a>
          </li>
        </ul>
      </li>
      <li class=" nav-item"><a href="#"><i class="la la-hdd-o"></i><span class="menu-title" data-i18n="nav.users.main">Master Data</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'data_barang') ? 'active' : ''); ?> nav-item">
            <a href="<?php echo base_url('barang/list_barang'); ?>"><span class="menu-title" data-i18n="nav.dash.main">Data Barang</span></a> 
          </li>
          <li class="<?php echo (($page == 'vendor') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('vendor/list_vendor'); ?>" data-i18n="nav.users.user_profile">Vendor</a>
          </li>
          <li class="<?php echo (($page == 'customer') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('customer'); ?>" data-i18n="nav.users.users_contacts">Customer</a>
          </li>
          <li class="<?php echo (($page == 'warehouse') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('warehouse'); ?>" data-i18n="nav.users.users_contacts">Warehouse</a>
          </li>
          <li class="<?php echo (($page == 'kategori') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('kategori'); ?>" data-i18n="nav.users.users_contacts">Kategori Barang</a>
          </li>
        </ul>
      </li>
      <li class="nav-item"><a href="#"><i class="la la-user"></i><span class="menu-title" data-i18n="nav.users.main">Users</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'tambah_user') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('users/tambah_user'); ?>" data-i18n="nav.users.user_profile">Tambah User</a>
          </li>
          <li class="<?php echo (($page == 'users') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('users'); ?>" data-i18n="nav.users.users_contacts">List User</a>
          </li>
        </ul>
      </li>
      <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.users.main">Setting</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'profile') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('profile'); ?>" data-i18n="nav.users.user_profile">Profile</a>
          </li>
          <li class="<?php echo (($page == 'ganti_password') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('profile/ganti_password'); ?>" data-i18n="nav.users.users_contacts">Ganti Password</a>
          </li>
        </ul>
      </li>
    </ul>
  
  <?php else: ?>  

  <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class="<?php echo ($page == 'dashboard' ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('dashboard'); ?>"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a> </li>
      <li class="<?php echo (($page == 'tambah_pengiriman') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('pengiriman/tambah_pengiriman'); ?>"><i class="la la-pencil-square"></i><span class="menu-title" data-i18n="nav.dash.main">Tambah Pengiriman</span></a> </li>
      <li class="<?php echo (($page == 'tambah_penerimaan') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('penerimaan/tambah_penerimaan'); ?>"><i class="la la-pencil"></i><span class="menu-title" data-i18n="nav.dash.main">Tambah Penerimaan</span></a> </li>
      <li class="<?php echo (($page == 'data_pengiriman' || $page == 'data_open'|| $page == 'data_proses' || $page == 'data_selesai') ? 'active' : ''); ?> nav-item"><a href="#"><i class="la la-truck"></i><span class="menu-title" data-i18n="nav.users.main">Data Pengiriman</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'data_open') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('pengiriman/data_open'); ?>" data-i18n="nav.users.user_profile">Open</a>
          </li>
          <li class="<?php echo (($page == 'data_proses') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('pengiriman/data_proses'); ?>" data-i18n="nav.users.users_contacts">Proses</a>
          </li>
          <li class="<?php echo (($page == 'data_selesai') ? 'active' : ''); ?>">
            <a class=" menu-item" href="<?php echo base_url('pengiriman/data_selesai'); ?>" data-i18n="nav.users.users_contacts">Selesai</a>
          </li>
        </ul>
      </li>
      <li class="<?php echo (($page == 'data_penerimaan') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('penerimaan'); ?>"><i class="la la-shopping-cart"></i><span class="menu-title" data-i18n="nav.dash.main">Data Penerimaan</span></a> </li>
      <li class="<?php echo (($page == 'stock_barang') ? 'active' : ''); ?> nav-item"><a href="<?php echo base_url('barang/stock_barang'); ?>"><i class="la la-cube"></i><span class="menu-title" data-i18n="nav.dash.main">Stock Barang</span></a> </li>
      
      <li class=" nav-item"><a href="#"><i class="la la-print"></i><span class="menu-title" data-i18n="nav.users.main">Laporan</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'laporan_barang_diterima') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('laporan/barang_diterima'); ?>" data-i18n="nav.users.user_profile">Barang Di Terima</a>
          </li>
          <li class="<?php echo (($page == 'laporan_barang_dikirim') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('laporan/barang_dikirim'); ?>" data-i18n="nav.users.users_contacts">Barang Di Kirim</a>
          </li>
          <li class="<?php echo (($page == 'laporan_stock_barang') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('laporan/stock_barang'); ?>" data-i18n="nav.users.users_contacts">Stock Barang</a>
          </li>
        </ul>
      </li>

      <li class=" nav-item"><a href="#"><i class="la la-hdd-o"></i><span class="menu-title" data-i18n="nav.users.main">Master Data</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'data_barang') ? 'active' : ''); ?> nav-item">
            <a href="<?php echo base_url('barang/list_barang'); ?>"><span class="menu-title" data-i18n="nav.dash.main">Data Barang</span></a> 
          </li>
          <li class="<?php echo (($page == 'vendor') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('vendor'); ?>" data-i18n="nav.users.user_profile">Vendor</a>
          </li>
          <li class="<?php echo (($page == 'customer') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('customer'); ?>" data-i18n="nav.users.users_contacts">Customer</a>
          </li>
          <li class="<?php echo (($page == 'kategori') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('kategori'); ?>" data-i18n="nav.users.users_contacts">Kategori Barang</a>
          </li>
        </ul>
      </li>
      <li class=" nav-item"><a href="#"><i class="la la-cog"></i><span class="menu-title" data-i18n="nav.users.main">Setting</span></a>
        <ul class="menu-content">
          <li class="<?php echo (($page == 'profile') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('profile'); ?>" data-i18n="nav.users.user_profile">Profile</a>
          </li>
          <li class="<?php echo (($page == 'ganti_password') ? 'active' : ''); ?>">
            <a class="menu-item" href="<?php echo base_url('profile/ganti_password'); ?>" data-i18n="nav.users.users_contacts">Ganti Password</a>
          </li>
        </ul>
      </li>
    </ul>

  <?php endif; ?>

  </div>
</div>
