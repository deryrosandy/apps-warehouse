<!-- REQUIRED JS SCRIPTS -->

<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->

  <!-- BEGIN PAGE VENDOR JS-->
  <script src="<?php echo base_url(); ?>assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>

<!-- BEGIN MODERN JS-->
<script src="<?php echo base_url(); ?>assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/core/app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END MODERN JS-->

<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/js/scripts/tables/datatables-extensions/datatable-responsive.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts/forms/select/form-select2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts/forms/form-repeater.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts/forms/checkbox-radio.js" type="text/javascript"></script>

<!-- My Script -->
<?php include './assets/js/script.php'; ?>