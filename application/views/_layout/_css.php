<link href="<?php echo base_url(); ?>assets/css/fonts.css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/fonts/line-awesome/css/line-awesome.min.css" rel="stylesheet">

<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/vendors.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/weather-icons/climacons.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/meteocons/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/charts/morris.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/charts/chartist.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/charts/chartist-plugin-tooltip.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendors/css/forms/icheck/custom.css">
<!-- END VENDOR CSS-->
<!-- BEGIN MODERN CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/app.css">
<!-- END MODERN CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/core/menu/menu-types/vertical-menu-modern.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/simple-line-icons/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/pages/timeline.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/pages/dashboard-ecommerce.css">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<!-- END Custom CSS-->