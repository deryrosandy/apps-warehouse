<!DOCTYPE html>
<html lang="en">
  <head>
    <?php /*<title><?php echo $this->general->getRestoName(); ?> | <?php echo ucwords(@$page ? $page:''); ?> - <?php echo ucwords(@$deskripsi ? $deskripsi:''); ?></title> */ ?>
    <title>Sistem Informasi Warehouse</title>
    <!-- meta -->
    <?php echo @$_meta; ?>

    <!-- css --> 
    <?php echo @$_css; ?>
  </head>

  <!--<body class="collapsed-menu expand-menu"> -->
  <body class="vertical-layout vertical-menu-modern 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
     
      <!-- header -->
      <?php echo @$_header; ?>

      <!-- sidebar -->
      <?php echo @$_sidebar; ?> <!-- nav -->

      <!-- right panel -->
      <?php //echo @$_right_panel; ?>
      
      <!-- content -->
      <?php echo @$_content; ?> <!-- headerContent --><!-- mainContent -->

      <!-- js -->
      <?php echo @$_js; ?>
	
  </body>
</html>