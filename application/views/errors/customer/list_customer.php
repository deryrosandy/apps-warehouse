<div class="msg" style="">
  <?php echo $this->session->flashdata('msg'); ?>
</div>

<div class="content-header row">
  <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block"><?php echo $judul; ?></h3>
    <div class="row breadcrumbs-top d-inline-block">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a>
          </li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('barang'); ?>"><?php echo $judul; ?></a>
          </li>
        </ol>
      </div>
    </div>
  </div>
  <div class="content-header-right col-md-6 col-12">
    <div class="dropdown float-md-right ">
      <a href="<?php echo base_url(); ?>customer/tambah_customer" class="btn btn-danger btn-glow px-2 round text-bold-500 white"><i class="ft-plus white"></i>Tambah Customer</a>
    </div>
  </div>
</div>

<div class="content-body">

  <!-- `new` constructor table -->
  <section id="constructor">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="card-content collapse show">
            <div class="card-body card-dashboard">
              <table class="table table-striped table-bordered dataex-res-constructor">
                <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama Customer</th>
                      <th>No. Telp</th>
                      <th>Alamat</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $no = 1; ?>
                      <?php foreach($customers as $customer): ?>
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $customer->customer_name; ?></td>
                        <td><?php echo $customer->phone_number; ?></td>
                        <td><?php echo $customer->alamat; ?></td>
                        <td>
                          <div class="btn-group btn-group-sm ">
                            <a href="<?php echo base_url(); ?>customer/update_customer/<?php echo $customer->customer_id; ?>" class="btn btn-warning"><i class="ft-edit white"></i></a>
                            <a id="" data-id="<?php echo $customer->customer_id; ?>" data-target="customer" class="btn_delete btn btn-danger"><i class="ft-trash-2 white"></i></a>
                          </div>
                        </td>
                        <?php $no++; ?>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- `new` constructor table -->
         
</div>