<div class="content-header row">
  <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">Ganti Password</h3>
    <div class="row breadcrumbs-top d-inline-block">
      <div class="breadcrumb-wrapper col-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Dashboard</a>
          </li>
          <li class="breadcrumb-item"><a href="#">Ganti password</a>
          </li>
        </ol>
      </div>
    </div>
  </div>
</div>

<div class="content-body">

  <section id="row-separator-form-layouts">
    <div class="row">
      
      <div class="col-md-9">
      
        <div class="msg" style="">
          <?php echo $this->session->flashdata('msg'); ?>
        </div>
        
        <div class="card">
          <div class="card-header">
            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>

          <div class="card-content collapse show">
            <div class="card-body">
              <form class="form form-horizontal row-separator" method="POST" action="<?php echo base_url(); ?>profile/ganti_password" >
                <div class="form-body">
                  <h4 class="form-section"><i class="la la-clipboard"></i> Ganti Password</h4>
                  <div class="form-group row">
                    <label class="col-md-4 label-control" for="product_code">Password Lama</label>
                    <div class="col-md-8">
                      <input type="password" id="product_code" value="" class="form-control" placeholder="Masukkan Password Lama" name="password_old"  data-title="Password Lama" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-4 label-control" for="product_code">Password Baru</label>
                    <div class="col-md-8">
                      <input type="password" id="" value="" class="form-control" placeholder="Masukkan Password Baru" name="password_new"  data-title="Password Baru" data-toggle="tooltip" data-trigger="hover" data-placement="top" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-4 label-control" for="product_code">Konfirmasi Password Baru</label>
                    <div class="col-md-8">
                      <input type="password" id="" value="" class="form-control" placeholder="Masukkan Kembali Password Baru" name="confirm_password"  data-title="Konfirmasi Password Baru" data-toggle="tooltip" data-trigger="hover" data-placement="top">
                    </div>
                  </div>
                </div>

                <input type="hidden" value="<?php echo $userdata->user_id; ?>" name="user_id" />
                
                <div class="pt-2 pb-2 pull-right">
                  <button type="button" class="btn btn-warning mr-1" onclick="history.back(-1)" >
                    <i class="la la-remove"></i> Cancel
                  </button>
                  <button value="flag" name="flag" type="submit" class="btn btn-primary">
                    <i class="la la-check"></i> Submit
                  </button>
                </div>
                 
              </form>
            </div>
          </div>

        </div>
      </div>

    </div>
  </section>

</div>
