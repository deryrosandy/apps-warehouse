<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_General extends CI_Model {

	public function get_user_by_id($user_id){

		$this->db->select('*'); 
		$this->db->from('users');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_all_vendor() {
		$sql = "SELECT * FROM vendor";
		
		$data = $this->db->query($sql);

		return $data->result();
	}
	
	public function get_all_customer() {
		$sql = "SELECT * FROM customer";
		
		$data = $this->db->query($sql);

		return $data->result();
	}
	
	public function get_customer_name() {
		$sql = "SELECT * FROM customer";
		
		$data = $this->db->query($sql);

		return $data->result();
	}
	
	public function get_all_penerimaan() {
		
		$sql = "SELECT * FROM receiving ORDER BY receiving_id ASC";
		
		$data = $this->db->query($sql);

		return $data->result();
	}
	
	public function get_all_pengiriman_open() {
		
		$this->db->where('status', 'open');
		$this->db->where('approved', 0);
        $query = $this->db->get('shipment');
		return $query->result();
	}
	public function get_all_pengiriman_approval() {
		
		$this->db->where('status', 'open');
		$this->db->or_where('status', 'process');
        $query = $this->db->get('shipment');
		return $query->result();
	}
	public function get_all_pengiriman_process() {
		
		$this->db->where('status', 'process');
		$this->db->where('approved', 1);
        $query = $this->db->get('shipment');
		return $query->result();
	}
	public function get_all_pengiriman_selesai() {
		
		$this->db->where('status', 'done');
		$this->db->where('approved', 1);
        $query = $this->db->get('shipment');
		return $query->result();
	}
	
	public function get_all_warehouse() {
		$sql = "SELECT * FROM warehouse ORDER BY warehouse_id ASC";
		
		$data = $this->db->query($sql);

		return $data->result();
	}
	
	public function get_all_product() {

		$this->db->select('a.*, tp.*');
		$this->db->from('product a');
		$this->db->join('product_category tp', 'tp.product_category_id = a.product_category_id', 'left');
		$query = $this->db->get();
		
		return $query->result();
	}

	public function get_all_stock_product() {

		$this->db->select('a.*, tp.*, tc.*');
		$this->db->from('product a');
		$this->db->join('product_stock tp', 'tp.product_id = a.product_id', 'left');
		$this->db->join('product_category tc', 'tc.product_category_id = a.product_category_id', 'left');
		$query = $this->db->get();
		
		return $query->result();
	}

	public function get_new_no_do() {

		$sql = "SELECT shipment_number FROM shipment ORDER BY shipment_id DESC limit 1";
		
		$data = $this->db->query($sql);
		$tahun = date('Y');
		$bulan = date('m');
		$nota_temp = (intval(substr($data->row('shipment_number'),10)) + 1);
		
		if($data->num_rows() > 0){
			$no_do = 'DO-' . $tahun . $bulan . ((substr($data->row('shipment_number'),9)) + 1);
		}else{
			$no_do = 'DO-' . $tahun . $bulan . '100001';
		}
		
		return $no_do;
		
	}

	public function insert_receiving($data){

		unset($data['flag']);
		
		$data['user_id'] = $this->userdata->user_id;
		$data_product 	= $data['product_list'];
		$data['created_at'] = date('Y-m-d H:i:s');
		unset($data['product_list']);

		$this->db->insert('receiving', $data);
		$receiving_id = $this->db->insert_id();
		
		foreach($data_product as $row){
			$row['receiving_id'] = $receiving_id;
			$result = $this->db->insert('product_receiving', $row);
			if($result == true){
				unset($row['receiving_id']);

				$this->db->where('product_id', $row['product_id']);
				$query = $this->db->get('product_stock');

				if($query->num_rows() > 0){
					$qty = $query->row('quantity');
					$new_qty = $qty+$row['quantity'];
					$row['quantity'] = $new_qty;
					$product_id = $row['product_id'];
					unset($row['product_id']);
					$this->db->where('product_id', $product_id);
					$result = $this->db->update('product_stock', $row);
				}else{
					$result = $this->db->insert('product_stock', $row);
				}

			}
		}

		return true;

	}

	public function insert_shipment($data){

		unset($data['flag']);
		
		$data['user_id'] = $this->userdata->user_id;
		$data_product 	= $data['product_list'];
		$data['created_at'] = date('Y-m-d H:i:s');
		unset($data['product_list']);

		$this->db->insert('shipment', $data);
		$shipment_id = $this->db->insert_id();
		
		foreach($data_product as $row){
			$row['shipment_id'] = $shipment_id;
			$result = $this->db->insert('product_shipment', $row);
			if($result == true){
				unset($row['shipment_id']);

				$this->db->where('product_id', $row['product_id']);
				$query = $this->db->get('product_stock');

				if($query->num_rows() > 0){
					$qty = $query->row('quantity');
					$new_qty = $qty-$row['quantity'];
					$row['quantity'] = $new_qty;
					$product_id = $row['product_id'];
					unset($row['product_id']);
					$this->db->where('product_id', $product_id);
					$result = $this->db->update('product_stock', $row);
				}else{
					$result = $this->db->insert('product_stock', $row);
				}

			}
		}

		return true;

	}

	public function get_all_product_category(){

		$this->db->select('*');
		$this->db->from('product_category');
		$this->db->order_by('product_category_id ASC');
		$query = $this->db->get();
		
		return $query->result();

	}

	public function delete_barang($id){

		$this->db->where('product_id', $id);
		$this->db->delete('product');

		return true;
	}

	public function delete_vendor($id){

		$this->db->where('vendor_id', $id);
		$this->db->delete('vendor');

		return true;
	}

	public function delete_kategori($id){

		$this->db->where('product_category_id', $id);
		$this->db->delete('product_category');

		return true;
	}

	public function delete_warehouse($id){

		$this->db->where('warehouse_id', $id);
		$this->db->delete('warehouse');

		return true;
	}

	public function delete_customer($id){

		$this->db->where('customer_id', $id);
		$this->db->delete('customer');

		return true;
	}

	public function get_receiving_by_id($receiving_id){
        
        $this->db->where('receiving_id', $receiving_id);
        $query = $this->db->get('receiving');
		return $query->row();
		
	}
	public function get_product_by_receving_id($receiving_id){
		
		$this->db->select('a.*, b.*'); 
		$this->db->from('product_receiving a');
		$this->db->join('product b', 'b.product_id = a.product_id', 'left');
        $this->db->where('a.receiving_id', $receiving_id);
		$query = $this->db->get();
		
		return $query->result();
		
	}
	public function get_product_by_shipment_id($shipment_id){
		
		$this->db->select('a.*, b.*'); 
		$this->db->from('product_shipment a');
		$this->db->join('product b', 'b.product_id = a.product_id', 'left');
        $this->db->where('a.shipment_id', $shipment_id);
        $query = $this->db->get();
		return $query->result();
		
	}
	public function get_receiving_list_by_date($start_date, $end_date){
		
		$this->db->select('a.*, b.*, c.*'); 
		$this->db->from('receiving a');
		$this->db->join('product_receiving b', 'b.receiving_id = a.receiving_id', 'left');
		$this->db->join('product c', 'c.product_id = b.product_id', 'left');
        $this->db->where('a.date_in >=', $start_date);
        $this->db->where('a.date_in <=', $end_date);
		$query = $this->db->get();
		
		//print_r($this->db->last_query()); die();
		
		return $query->result();		
	}
	public function get_shipment_list_by_date($start_date, $end_date){
		
		$this->db->select('a.*, b.*, c.*'); 
		$this->db->from('shipment a');
		$this->db->join('product_shipment b', 'b.shipment_id = a.shipment_id', 'left');
		$this->db->join('product c', 'c.product_id = b.product_id', 'left');
        $this->db->where('a.date_out >=', $start_date);
        $this->db->where('a.date_out <=', $end_date);
		$query = $this->db->get();
		
		//print_r($this->db->last_query()); die();
		
		return $query->result();		
	}

	public function insert_product($data){

		unset($data['flag']);

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['user_id'] = $this->userdata->user_id;
		
		$result = $this->db->insert('product', $data);

		$insert_id = $this->db->insert_id();

		if($result==true){

			$datas['product_id'] = $insert_id;
			$datas['quantity'] = 0;
		
			$result = $this->db->insert('product_stock', $datas);

			if($result==true){
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

	public function update_product($data, $product_id){

		unset($data['flag']);

		$this->db->where('product_id', $product_id);
		$result = $this->db->update('product', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function update_product_stock($data, $product_stock_id){

		unset($data['flag']);
		
		$this->db->where('product_stock_id', $product_stock_id);
		$result = $this->db->update('product_stock', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_vendor($data){

		unset($data['flag']);

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['user_id'] = $this->userdata->user_id;
		
		$result = $this->db->insert('vendor', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_warehouse($data){

		unset($data['flag']);
		
		$result = $this->db->insert('warehouse', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function update_warehouse($data, $warehouse_id){

		unset($data['flag']);

		$this->db->where('warehouse_id', $warehouse_id);
		$result = $this->db->update('warehouse', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_kategori($data){

		unset($data['flag']);
		
		$result = $this->db->insert('product_category', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function update_kategori($data, $kategori_id){

		unset($data['flag']);

		$this->db->where('product_category_id', $kategori_id);
		$result = $this->db->update('product_category', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function update_vendor($data, $product_id){

		unset($data['flag']);

		$this->db->where('vendor_id', $product_id);
		$result = $this->db->update('vendor', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_customer($data){

		unset($data['flag']);

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['user_id'] = $this->userdata->user_id;
		
		$result = $this->db->insert('customer', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function update_customer($data, $customer_id){

		unset($data['flag']);

		$this->db->where('customer_id', $customer_id);
		$result = $this->db->update('customer', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function update_receiving($data, $receiving_id){

		unset($data['flag']);
		
		$data_product 	= $data['product_list'];
		unset($data['product_list']);

		$this->db->where('receiving_id', $receiving_id);
		$result = $this->db->update('receiving', $data);

		foreach($data_product as $row){
			
			$this->db->where('receiving_id', $receiving_id);
			$this->db->where('product_id', $row['product_id']);
			$query = $this->db->get('product_receiving');
			$qty_old = $query->row('quantity');			

			$this->db->where('product_id', $row['product_id']);
			$query = $this->db->get('product_stock');

			if($query->num_rows() > 0){
				$new_qty = $query->row('quantity')-$qty_old;
				$data_stock['quantity'] = $new_qty;
				$product_id = $row['product_id'];

				$this->db->where('product_id', $product_id);
				$result = $this->db->update('product_stock', $data_stock);
			}			

			$this->db->where('receiving_id', $receiving_id);
			$this->db->where('product_id', $row['product_id']);
			$result_delete = $this->db->delete('product_receiving');

			$row['receiving_id'] = $receiving_id;
			$result = $this->db->insert('product_receiving', $row);
			if($result == true){
				unset($row['receiving_id']);

				$this->db->where('product_id', $row['product_id']);
				$query = $this->db->get('product_stock');

				if($query->num_rows() > 0){
					$qty = $query->row('quantity');
					$new_qty = $qty+$row['quantity'];
					$row['quantity'] = $new_qty;
					$product_id = $row['product_id'];
					unset($row['product_id']);
					$this->db->where('product_id', $product_id);
					$result = $this->db->update('product_stock', $row);
				}else{
					$result = $this->db->insert('product_stock', $row);
				}
			}
		}

		return $result;

	}

	public function update_shipment($data, $shipment_id){

		unset($data['flag']);
		
		$data_product 	= $data['product_list'];
		unset($data['product_list']);

		$this->db->where('shipment_id', $shipment_id);
		$result = $this->db->update('shipment', $data);

		foreach($data_product as $row){
			
			$this->db->where('shipment_id', $shipment_id);
			$this->db->where('product_id', $row['product_id']);
			$query = $this->db->get('product_shipment');
			$qty_old = $query->row('quantity');			

			$this->db->where('product_id', $row['product_id']);
			$query = $this->db->get('product_stock');

			if($query->num_rows() > 0){
				$new_qty = $query->row('quantity')+$qty_old;
				$data_stock['quantity'] = $new_qty;
				$product_id = $row['product_id'];

				$this->db->where('product_id', $product_id);
				$result = $this->db->update('product_stock', $data_stock);
			}			

			$this->db->where('shipment_id', $shipment_id);
			$this->db->where('product_id', $row['product_id']);
			$result_delete = $this->db->delete('product_shipment');

			$row['shipment_id'] = $shipment_id;
			$result = $this->db->insert('product_shipment', $row);
			if($result == true){
				unset($row['shipment_id']);

				$this->db->where('product_id', $row['product_id']);
				$query = $this->db->get('product_stock');

				if($query->num_rows() > 0){
					$qty = $query->row('quantity');
					$new_qty = $qty-$row['quantity'];
					$row['quantity'] = $new_qty;
					$product_id = $row['product_id'];
					unset($row['product_id']);
					$this->db->where('product_id', $product_id);
					$result = $this->db->update('product_stock', $row);
				}else{
					$result = $this->db->insert('product_stock', $row);
				}
			}
		}

		return $result;

	}


	public function approval_shipment($data, $shipment_id){

		$data['status'] = $data['status'];
		
		$this->db->where('shipment_id', $shipment_id);
		$result = $this->db->update('shipment', $data);

		if($result===true){
			return true;
		}else{
			return false;
		}
		
	}

	public function get_product_by_id($product_id){

		$this->db->select('*'); 
		$this->db->from('product');
		$this->db->where('product_id', $product_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_category_by_id($category_id){

		$this->db->select('*'); 
		$this->db->from('product_category');
		if($category_id !=='all'){
			$this->db->where('product_category_id', $category_id);
		}
		$query = $this->db->get();

		return $query->row();
	}

	public function get_product_stock_by_id($product_id){

		$this->db->select('*'); 
		$this->db->from('product_stock a');
		$this->db->join('product b', 'b.product_id = a.product_id', 'left');
		$this->db->where('a.product_stock_id', $product_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_vendor_by_id($vendor_id){

		$this->db->select('*'); 
		$this->db->from('vendor');
		$this->db->where('vendor_id', $vendor_id);
		$query = $this->db->get();

		return $query->row();
	}
	
	public function get_customer_by_id($customer_id){

		$this->db->select('*'); 
		$this->db->from('customer');
		$this->db->where('customer_id', $customer_id);
		$query = $this->db->get();

		return $query->row();
	}
	
	public function get_warehouse_by_id($warehouse_id){

		$this->db->select('*'); 
		$this->db->from('warehouse');
		$this->db->where('warehouse_id', $warehouse_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_kategori_by_id($kategori_id){

		$this->db->select('*'); 
		$this->db->from('product_category');
		$this->db->where('product_category_id', $kategori_id);
		$query = $this->db->get();

		return $query->row();
	}
	
	public function get_product_stock_by_category($kategori_id){

		$this->db->select('a.*, b.*'); 
		$this->db->from('product a');
		$this->db->join('product_stock b', 'b.product_id = a.product_id', 'left');
		if($kategori_id !=='all'){
			$this->db->where('a.product_category_id', $kategori_id);
		}
		$query = $this->db->get();

		return $query->result();
	}
	
	
	public function update_password($data){

		$this->db->where('user_id', $data['user_id']);
		$this->db->where('password', md5($data['password_old']));
		$query = $this->db->get('users'); 
		$result = $query->num_rows();
		
		if($result > 0 && ($data['password_new']==$data['confirm_password'])){
			$user_id 			= $data['user_id'];
			$data['password'] 	= md5($data['confirm_password']);
			
			unset($data['flag']);
			unset($data['user_id']);
			unset($data['password_old']);
			unset($data['password_new']);
			unset($data['confirm_password']);
			
			$this->db->where('user_id', $user_id);
			$result = $this->db->update('users', $data); 
			
			if($result==true){
				return true;
			}else{
				return false;
			}
		}	

	}
	
	public function get_all_pengiriman() {
		$sql = "SELECT * FROM shipment";
		$data = $this->db->query($sql);
		return $data->result();
	}

	public function get_shipment_by_id($shipment_id){
        
        $this->db->select('*'); 
		$this->db->from('shipment');
        $this->db->where('shipment_id', $shipment_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_therapist_detail_by_kodekar($kodekar){
        $sql="SELECT a.kodetrp, a.tglmasuk, a.kodekar, a.levelkomisi, a.namakar, b.saldo_terakhir, a.no_rek, a.bank
				FROM tkar a
				LEFT JOIN tabungan b ON b.kodekar = a.kodekar
				WHERE a.kodekar = '$kodekar'
				ORDER BY a.kodekar ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_massage_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_massage
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_services_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_services
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_kedisiplinan_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_kedisiplinan
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_kesehatan_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_kesehatan
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_sikap_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_sikap
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_photo_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_image
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_performance_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_performance
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_detail_by_kodekar_month($kodekar, $report_month){
        $this->db->select('a.*, tp.*');
		$this->db->from('therapist a');
		$this->db->join('therapist_placement tp', 'tp.therapist_id = a.therapist_id', 'left');
		$this->db->where('a.therapist_id', $kodekar);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	}

	public function get_therapist_grade_by_kodekar_month($kodekar, $report_month){
        $sql="SELECT *
				FROM therapist_grade
				WHERE therapist_id = '$kodekar'
				AND month = '$report_month'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_massage_by_kodekar_year($kodekar, $year){
		$sql="SELECT *
		FROM therapist_massage
		WHERE therapist_id = '$kodekar'
		AND LEFT(month, 4) = '$year'
		GROUP BY month
		ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_services_by_kodekar_year($kodekar, $year){
		$sql="SELECT *
		FROM therapist_services
		WHERE therapist_id = '$kodekar'
		AND LEFT(month, 4) = '$year'
		GROUP BY month
		ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_kedisiplinan_by_kodekar_year($kodekar, $year){
		$sql="SELECT *
		FROM therapist_kedisiplinan
		WHERE therapist_id = '$kodekar'
		AND LEFT(month, 4) = '$year'
		GROUP BY month
		ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_kesehatan_by_kodekar_year($kodekar, $year){
        $sql="SELECT *
				FROM therapist_kesehatan
				WHERE therapist_id = '$kodekar'
				AND LEFT(month, 4) = '$year'
				GROUP BY month
				ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_sikap_by_kodekar_year($kodekar, $year){
        $sql="SELECT *
				FROM therapist_sikap
				WHERE therapist_id = '$kodekar'
				AND LEFT(month, 4) = '$year'
				GROUP BY month
				ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_performance_by_kodekar_year($kodekar, $year){
		$sql="SELECT *
		FROM therapist_performance
		WHERE therapist_id = '$kodekar'
		AND LEFT(month, 4) = '$year'
		GROUP BY month
		ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}

	}

	public function get_therapist_grade_by_kodekar_year($kodekar, $year){
		$sql="SELECT *
		FROM therapist_grade
		WHERE therapist_id = '$kodekar'
		AND LEFT(month, 4) = '$year'
		GROUP BY month
		ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_photo_by_kodekar_year($kodekar, $year){
		$sql="SELECT *
		FROM therapist_image
		WHERE therapist_id = '$kodekar'
		AND LEFT(month, 4) = '$year'
		GROUP BY month
		ORDER BY month ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_massage_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_massage
				WHERE therapist_id = '$kodekar'				
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_grade_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_grade
				WHERE therapist_id = '$kodekar'				
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_photo_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_image
				WHERE therapist_id = '$kodekar'				
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	public function get_therapist_services_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_services
				WHERE therapist_id = '$kodekar'
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_kedisiplinan_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_kedisiplinan
				WHERE therapist_id = '$kodekar'
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_kesehatan_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_kesehatan
				WHERE therapist_id = '$kodekar'
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_sikap_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_sikap
				WHERE therapist_id = '$kodekar'
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_performance_by_kodekar_last($kodekar){
        $sql="SELECT *
				FROM therapist_performance
				WHERE therapist_id = '$kodekar'
				Order By month DESC
				LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return $query->row();
		}
	}

	public function get_therapist_detail_by_kodekar_last($kodekar){
    	$this->db->select('a.*, tp.*');
		$this->db->from('therapist a');
		$this->db->join('therapist_placement tp', 'tp.therapist_id = a.therapist_id', 'left');
		$this->db->where('a.therapist_id', $kodekar);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	}

	public function insert_therapist($data){

		unset($data['flag']);

		$data['created_at'] = date('Y-m-d H:i:s');

		$result = $this->db->insert('therapist', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}

	}



	public function get_grade_therapist_by_filter($dari_tanggal, $sampai_tanggal, $branch_id){
        
		$sql="Select branch_code,kodekar, namakar, total_tamu, total_ts, ts_persen, ts_nilai, point, ratio_nilai, gc_comment, gc_comment_persen, gc_comment_nilai, hasil, grade, gc_performance, gc_massage, gc_service, jml_gc
			From grade_therapist
			Where periode= '$dari_tanggal'
			AND  branch_code='$branch_id'";
				
        $query=$this->db->query($sql);
		
		return $query->result();

    }
	
	public function get_guest_comment_therapist_by_filter($dari_tanggal, $sampai_tanggal, $branch_id){  
		$sql="Select DISTINCT a.kodekar, b.namakar, MIN(a.tanggal) as awal,
		MAX(a.tanggal) as akhir, count(DISTINCT(noreg)) as total_tamu, sum(extend) as point, a.branch_code,
		(Select count(DISTINCT(noreg)) as total_tamu From rpt_therapist.dbo.registry Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tanggal >= '$dari_tanggal' AND tanggal <= '$sampai_tanggal' AND statustrp='TS') as total_ts,
		(Select Sum(Optbqpj) From rpt_therapist.dbo.loggc Where tanggal >= '$dari_tanggal' AND tanggal <= '$sampai_tanggal' AND branch_code = '$branch_id' AND kodekar=a.kodekar) as performance,
		(Select Sum(Optbqpn) From rpt_therapist.dbo.loggc Where tanggal >= '$dari_tanggal' AND tanggal <= '$sampai_tanggal' AND branch_code = '$branch_id' AND kodekar=a.kodekar) as massage,
		(Select Sum(Optbqs) From rpt_therapist.dbo.loggc Where tanggal >= '$dari_tanggal' AND tanggal <= '$sampai_tanggal' AND branch_code = '$branch_id' AND kodekar=a.kodekar) as service,
		(Select count(DISTINCT(noreg)) From rpt_therapist.dbo.loggc Where tanggal >= '$dari_tanggal' AND tanggal <= '$sampai_tanggal' AND branch_code = '$branch_id' AND kodekar=a.kodekar) as jumlah_guest_comment
		From rpt_therapist.dbo.registry a
		JOIN rpt_therapist.dbo.tkar b ON b.kodekar = a.kodekar
		Where a.branch_code = '$branch_id'
		AND a.tanggal >= '$dari_tanggal'
		AND a.tanggal <= '$sampai_tanggal'
		GROUP BY a.kodekar, b.namakar, a.branch_code
		ORDER BY kodekar ASC";
				
        $query=$this->db->query($sql);
		
		//print_r($this->db->last_query()); die();
		//print_r($query->result()); die();
		return $query->result();		

    }

	public function get_therapist_detail_by_kodetrp($kodetrp){
        $sql="SELECT a.kodetrp, a.tglmasuk, a.branch_code, a.kodekar, a.levelkomisi, a.namakar, b.saldo_terakhir, a.no_rek, a.bank
				FROM tkar a
				LEFT JOIN tabungan b ON b.kodekar = a.kodekar
				WHERE kodetrp = '$kodetrp'
				ORDER BY a.kodekar ASC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	}

	public function get_therapist_by_kodekar($kodekar){
		$sql="SELECT *
			FROM therapist
			Where therapist_id = '$kodekar'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	}

	public function mutasi_therapist($therapist_id, $branch_id){
		$date = date('Y-m-d H:i:s');
		$this->db->set('branch_id', $branch_id);
		$this->db->set('update_at', $date);
		$this->db->where('therapist_id', $therapist_id);
		$result = $this->db->update('therapist_placement');

		// /print_r($this->db->last_query()); die();
		
		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function get_tabungan_therapist_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_code){
        $sql="SELECT setoran
				FROM tabungan
				WHERE kodekar = '$kodekar'
				AND branch_code = '$branch_code'
				AND tanggal >= '$dari_tanggal'
				AND tanggal <= '$sampai_tanggal'";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row('setoran');
		}else{
			return 0;
		}
	}

	public function get_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_code){
        $sql="SELECT *
				FROM tabungan
				WHERE kodekar = '$kodekar'
				AND branch_code = '$branch_code'
				AND tanggal >= '$dari_tanggal'
				AND tanggal <= '$sampai_tanggal'";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	}

	public function get_approval_registry($dari_tanggal, $sampai_tanggal, $branch_id){
        $sql="SELECT *
				FROM registry_approval
				WHERE branch_code = '$branch_id'
				AND dari_tanggal = '$dari_tanggal'
				AND sampai_tanggal = '$sampai_tanggal'";

		$query = $this->db->query($sql);

		return $query->row();
	}

	public function save_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $jumlah_tabungan, $branch_id){
		$tabungan_old = $this->get_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id);
		if($tabungan_old == null){
			$data = array(
				'kodekar' => $kodekar,
				'tanggal' => $dari_tanggal,
				'setoran' => $jumlah_tabungan,
				'branch_code' => $branch_id
			);
			$this->db->where('kodekar', $kodekar);
			$this->db->where('tanggal', $dari_tanggal);
			$q = $this->db->get('tabungan');
			$this->db->reset_query();
			
			if ( $q->num_rows() < 1){

				$data = $this->db->insert('tabungan', $data);
		
				if($data==true){
					return 'Tabungan Berhasil Di Masukkan';
				}else{
					return 'Tabungan Gagal Di Masukkan';
				}
			}
		
		}else{
			$data = array(
				'setoran' => $jumlah_tabungan
			);

			$this->db->where('kodekar', $kodekar);
			$this->db->where('branch_code', $branch_id);
			$this->db->where('tanggal >=', $dari_tanggal);
			$this->db->where('tanggal <=', $sampai_tanggal);
    		$this->db->update('tabungan', $data);
			return true;
		}
	}
	
	public function set_approval_registry($dari_tanggal, $sampai_tanggal, $branch_id){
		$data_old = $this->get_approval_registry($dari_tanggal, $sampai_tanggal, $branch_id);
		
		if($data_old == null){
			$data = array(
				'branch_code' => $branch_id,
				'sampai_tanggal' => $sampai_tanggal,
				'dari_tanggal' => $dari_tanggal,
				'created' => date('Y-m-d H:i:s'),
				'users_id' => $this->userdata->user_id_user,
				'status' => 1,
			);
			
			//var_dump($data); die();
			$data = $this->db->insert('registry_approval', $data);
	
			if($data==true){
				return 'Data Berhasil Di Masukkan';
			}else{
				return 'Data Gagal Di Masukkan';
			}
		}
	}

	public function get_branch_list(){
		$this->db->select('*'); 
		$this->db->from('branch');
		$query = $this->db->get();

		return $query->result();
		
	}

	public function get_therapist_by_branch_id($branch_id){
		
		if($branch_id =='all'){
			$this->db->select('a.*');
			$this->db->from('therapist a');
			$this->db->join('therapist_placement tp', 'tp.therapist_id = a.therapist_id', 'left');
			$this->db->order_by('a.therapist_id', 'DESC');
			$query = $this->db->get(); 
			$therapist = $query->result();
		}elseif($branch_id ==0){
			$this->db->select('a.*');
			$this->db->from('therapist a');
			$this->db->join('therapist_placement tp', 'tp.therapist_id = a.therapist_id', 'left');
			$this->db->order_by('a.therapist_id', 'DESC');
			$query = $this->db->get(); 
			$therapist = $query->result();
		}else{
			$this->db->select('a.*');
			$this->db->from('therapist a');
			$this->db->join('therapist_placement tp', 'tp.therapist_id = a.therapist_id', 'left');
			$this->db->where('tp.branch_id', $branch_id);
			$this->db->order_by('a.therapist_id', 'DESC');
			$query = $this->db->get(); 
			$therapist = $query->result();               
		}		
    return $therapist;
	}

	public function get_double_outlet_therapist($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="SELECT DISTINCT(kodekar), branch_code
			FROM registrydet
			WHERE kodekar = '$kodekar'
			AND tgl >= '$dari_tanggal'
			AND tgl <= '$sampai_tanggal'
			GROUP BY kodekar, branch_code";
		
		$query = $this->db->query($sql);
		
        return $query->num_rows();
	}

	public function get_therapist_tabungan_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id){
		
		$sql="SELECT DISTINCT(a.kodekar), b.*
			FROM tabungan a
			LEFT JOIN tkar b ON b.kodekar = a.kodekar
			WHERE b.therapist = 1
			AND b.trpozon = 0
			AND b.kodekar != 1
			AND b.kodekar != 0
			AND a.tanggal >= '$dari_tanggal'		
			AND a.tanggal <= '$sampai_tanggal'		
			AND a.kodekar IN (
					SELECT DISTINCT(kodekar)
						FROM registrydet
						WHERE branch_code = '$branch_id'
						AND tgl >= '$dari_tanggal'		
						AND tgl <= '$sampai_tanggal'
						AND status = 'Y')
			AND a.kodekar NOT IN (
					SELECT DISTINCT(kodekar)
						FROM tabungan
						Where branch_code != '$branch_id'
						AND tanggal >= '$dari_tanggal'
						AND tanggal <= '$sampai_tanggal')
			ORDER BY b.kodekar ASC";
		
		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
        return $query->result();
	
	}

	public function get_therapist_tabungan_count_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id){
		
		$sql="SELECT DISTINCT(a.kodekar), b.*
			FROM tabungan a
			LEFT JOIN tkar b ON b.kodekar = a.kodekar
			WHERE b.therapist = 1
			AND b.trpozon = 0
			AND b.kodekar != 1
			AND b.kodekar != 0
			AND a.tanggal >= '$dari_tanggal'		
			AND a.tanggal <= '$sampai_tanggal'		
			AND a.kodekar IN (
					SELECT DISTINCT(kodekar)
						FROM registrydet
						WHERE branch_code = '$branch_id'
						AND tgl >= '$dari_tanggal'		
						AND tgl <= '$sampai_tanggal'
						AND status = 'Y')
			AND a.kodekar NOT IN (
					SELECT DISTINCT(kodekar)
						FROM tabungan
						Where branch_code != '$branch_id'
						AND tanggal >= '$dari_tanggal'
						AND tanggal <= '$sampai_tanggal')
			ORDER BY b.kodekar ASC";
		
		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
        return $query->num_rows();
	
	}

	public function get_gro_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id){

		$sql="Select kodegro, namagro
				From registry_gro
				Where tanggal >= '$dari_tanggal'
				AND tanggal <= '$sampai_tanggal'
				AND branch_code = '$branch_id'
			GROUP BY kodegro, namagro";

		$query = $this->db->query($sql);
        return $query->result();
	}

	public function get_penjualan_gro_by_branch_id($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro){
		/*
		$sql="Select kodeservice, namaservice,  COUNT(*) as jumlah
				From registry_gro
				Where tanggal >=  '$dari_tanggal'
				AND tanggal <= '$sampai_tanggal'
				AND kodegro = '$kodegro'
				AND branch_code = '$branch_id'
				GROUP BY namaservice, kodeservice";

		$query = $this->db->query($sql);
		return $query->result();
		*/
		$this->db->select("add_product.komisi_gro, registry_gro.kodeservice, registry_gro.namaservice,  COUNT(*) as jumlah"); 
		$this->db->from("registry_gro");
		$this->db->join ("add_product", "add_product.kodeprod = registry_gro.kodeservice AND add_product.branch_code = '" . $branch_id . "'");
		$this->db->where("registry_gro.kodegro", $kodegro);	
		$this->db->where("registry_gro.tanggal >=", $dari_tanggal);	
		$this->db->where("registry_gro.tanggal <=", $sampai_tanggal);
		$this->db->where("registry_gro.branch_code", $branch_id);
		$this->db->group_by("registry_gro.kodeservice, registry_gro.namaservice, add_product.komisi_gro");
		$this->db->order_by("registry_gro.kodeservice ASC");
		$query = $this->db->get();

		//print_r($this->db->last_query()); die();

		return $query->result();		
	
	}

	public function get_all_user(){

		$this->db->select('*'); 
		$this->db->from('users');
		$query = $this->db->get();

		return $query->result();
	}	

	public function get_all_category(){

		$this->db->select('*'); 
		$this->db->from('product_category');
		$query = $this->db->get();

		return $query->result();
	}	

	public function get_gro_period($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select DISTINCT(kodegro), namagro
			From registry_gro
			WHERE branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'
			GROUP BY kodegro, namagro";

			$query = $this->db->query($sql);
			
			return $query->result();
	}

	public function get_total_hot_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro){
		$sql="Select COUNT(*) as total
			FROM registry_gro
			WHere namaservice LIKE 'hot%'
			AND kodegro = '$kodegro'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			//print_r($this->db->last_query()); die();

			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
	}
	
	public function get_total_aroma_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro){
		$sql="Select COUNT(*) as total
			FROM registry_gro
			WHere namaservice LIKE 'aroma%'
			AND kodegro = '$kodegro'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
	}
	
	public function get_total_lulur_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro){
		$sql="Select COUNT(*) as total
			FROM registry_gro
			WHere namaservice LIKE 'lulur%'
			AND kodegro = '$kodegro'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
	}
	
	public function get_total_mud_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro){
		$sql="Select COUNT(*) as total
			FROM registry_gro
			WHere namaservice LIKE 'mud%'
			AND kodegro = '$kodegro'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
	}
	
	public function get_total_suite_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro){

		$sql="Select count(DISTINCT(noreg)) as total
			FROM registry_gro
			WHere kodeservice='SUI'
			AND kodegro = '$kodegro'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'
			AND status = 'Y'";

			$query = $this->db->query($sql);

			//print_r($this->db->last_query()); die();
			
			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
	}
	
	public function get_total_point_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro){
		$total_hot = $this->get_total_hot_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);
		$total_aroma = $this->get_total_aroma_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);
		$total_mud = $this->get_total_mud_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id,  $kodegro);
		$total_lulur = $this->get_total_lulur_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);
		$total_suite = $this->get_total_suite_by_kodegro($dari_tanggal, $sampai_tanggal, $branch_id, $kodegro);

		$total_point = ($total_suite*10)+($total_aroma*5)+($total_mud*50)+($total_lulur*10)+($total_hot*10);

		return $total_point;
	}
	
	public function get_total_hotstone($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select COUNT(*) as total_hot
			FROM registry_gro
			WHere namaservice LIKE 'hot%'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			//print_r($this->db->last_query()); die();

			if($query->num_rows() > 0){
				return $query->row()->total_hot;
			}else{
				return 0;
			}
	}

	public function get_total_aroma($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select COUNT(*) as total_aroma
			FROM registry_gro
			WHere namaservice LIKE 'aroma%'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			//print_r($this->db->last_query()); die();

			if($query->num_rows() > 0){
				return $query->row()->total_aroma;
			}else{
				return 0;
			}
	}

	public function get_total_lulur($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select COUNT(*) as total_lulur
			FROM registry_gro
			WHere namaservice LIKE 'lulur%'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->total_lulur;
			}else{
				return 0;
			}
	}

	public function get_total_mud($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select COUNT(*) as total_mud
			FROM registry_gro
			WHere namaservice LIKE 'mud%'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->total_mud;
			}else{
				return 0;
			}
	}

	public function get_total_suite($dari_tanggal, $sampai_tanggal, $branch_id){

		$sql="Select count(DISTINCT(noreg)) as total
			FROM registry_gro
			WHere kodeservice='SUI'
			AND branch_code = '$branch_id'
			AND tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'
			AND status = 'Y'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
	}

	public function get_total_point_all($dari_tanggal, $sampai_tanggal, $branch_id){
		$total_hot = $this->get_total_hotstone($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_aroma = $this->get_total_aroma($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_lulur = $this->get_total_lulur($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_mud = $this->get_total_mud($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_suite = $this->get_total_suite($dari_tanggal, $sampai_tanggal, $branch_id);

		$total_point_all = ($total_suite*10)+($total_aroma*5)+($total_mud*50)+($total_lulur*10)+($total_hot*10);

		return $total_point_all;
	}

	public function get_point_per_gro($dari_tanggal, $sampai_tanggal, $branch_id){
		$total_hot = $this->get_total_hotstone($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_aroma = $this->get_total_aroma($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_lulur = $this->get_total_lulur($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_mud = $this->get_total_mud($dari_tanggal, $sampai_tanggal, $branch_id);
		$total_suite = $this->get_total_suite($dari_tanggal, $sampai_tanggal, $branch_id);

		$total_point_all = ($total_suite*10)+($total_aroma*5)+($total_mud*50)+($total_lulur*10)+($total_hot*10);

		return $total_point_all;
	}

	public function get_komisi_hotstone($status){
		$sql="Select CONVERT(int, $status) as komisi
			FROM komisi_prod_gro
			Where kode LIKE 'hot%'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->komisi;
				//var_dump($query->row()->$status); die();
			}else{
				return 0;
			}
	}

	public function get_komisi_aroma(){
		$sql="Select CONVERT(int, komisi_1) as komisi
			FROM komisi_prod_gro
			Where kode LIKE 'aroma%'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->komisi;
			}else{
				return 0;
			}
	}

	public function get_komisi_mud(){
		$sql="Select CONVERT(int, komisi_1) as komisi
			FROM komisi_prod_gro
			Where kode LIKE 'mud%'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->komisi;
			}else{
				return 0;
			}
	}

	public function get_komisi_lulur(){
		$sql="Select CONVERT(int, komisi_1) as komisi
			FROM komisi_prod_gro
			Where kode LIKE 'lulur%'";

			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0){
				return $query->row()->komisi;
			}else{
				return 0;
			}
	}

	public function get_total_treatment($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select COUNT(DISTINCT(noreg)) as total_treatment
			From registry_gro
			WHERE tanggal >= '$dari_tanggal'
			AND tanggal <= '$sampai_tanggal'
			AND kodegro IN (Select kodegro
								From registry_gro
								Where tanggal >= '$dari_tanggal'
								AND tanggal <= '$sampai_tanggal'
								AND branch_code = '$branch_id'
							GROUP BY kodegro)
			AND status='Y'
			AND branch_code = '$branch_id'";

			$query = $this->db->query($sql);

			//print_r($this->db->last_query()); die();
			
			if($query->num_rows() > 0){
				return $query->row()->total_treatment;
			}else{
				return 0;
			}
	}

	public function get_user_by_kodekar($kodekar){

		$this->db->select('*'); 
		$this->db->where('user_id', $kodekar);
		$this->db->from('users');
		$query = $this->db->get();

		return $query->row();
	}

	public function get_potongan_therapist($dari_tanggal, $sampai_tanggal, $branch_id){

		$sql="SELECT a.id_potongan, a.kodekar, yayasan, asuransi, koperasi, zion, denda, hutang, lain_lain, tiket, sertifikasi, trt_glx, bpjs_kry, bpjs_prs, b.kodetrp, b.levelkomisi, b.namakar
				FROM potongan_gaji a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

		$query = $this->db->query($sql);
		/*
		if($query->num_rows() == 0){
			$sql="INSERT INTO potongan_gaji (kodekar, tanggal, created, branch_code, yayasan, asuransi)
				Select DISTINCT a.kodekar, MAX(a.tgl) as tgl, MAX(a.tgl) as tgl2, branch_code,
				(Select yayasan From potongan_param) as yayasan,
				(Select asuransi From potongan_param) as asuransi
				From sesi_therapist a
				JOIN tkar b ON b.kodekar = a.kodekar
				Where branch_code = '$branch_id'
				AND a.tgl  >= '$dari_tanggal'
				AND a.tgl <= '$sampai_tanggal'
				GROUP BY a.kodekar, branch_code
				ORDER BY kodekar ASC";
			
				$this->db->query($sql);

			$sql="SELECT  a.id_potongan, a.kodekar, yayasan, asuransi, koperasi, mess, zion, denda, hutang, lain_lain, tiket, sertifikasi, trt_glx, bpjs_kry, bpjs_prs, b.kodetrp, b.levelkomisi, b.namakar
				FROM potongan_gaji a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

			$query = $this->db->query($sql);
		}
		*/
		//print_r($this->db->last_query()); die();
		return $query->result();
	}

	public function get_potongan_spv_therapist($dari_tanggal, $sampai_tanggal, $branch_id){

		$sql="SELECT a.id_potongan, a.id_spv, a.kodekar, yayasan, asuransi, koperasi, zion, denda, hutang, lain_lain, tiket, sertifikasi, trt_glx, bpjs_kry, bpjs_prs, b.nama_spv, b.is_mess
				FROM potongan_gaji a
				LEFT JOIN spv_therapist b ON b.id_spv = a.id_spv
				WHERE b.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.id_spv ASC";

		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
		return $query->result();
	}

	public function get_tabungan_therapist($dari_tanggal, $sampai_tanggal, $branch_id){

		$sql="SELECT a.id_potongan, a.kodekar, yayasan, asuransi, koperasi, zion, denda, hutang, lain_lain, tiket, sertifikasi, trt_glx, bpjs_kry, bpjs_prs, b.kodetrp, b.levelkomisi, b.namakar
				FROM potongan_gaji a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
		return $query->result();
	}
	
	public function get_branch_by_branch_id($branch_id){
        
        $this->db->select('*'); 
		$this->db->from('branch');
        $this->db->where('csname', $branch_id);
        $query = $this->db->get();
		return $query->row();
		
	}

	public function data_komisi_fnb_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal){
        
       	$sql="SELECT DISTINCT kodeprod, komisi_trp, namaprod, jumlah
            FROM add_product
            INNER JOIN (
                SELECT  point_additional.kodeservice, SUM(point_additional.jml) as jumlah, point_additional.branch_code as branch
            FROM point_additional
            WHERE point_additional.kodekar = '$kodekar'
            AND point_additional.tgl >= '$dari_tanggal'
            AND point_additional.tgl <= '$sampai_tanggal'
            GROUP BY point_additional.kodeservice, point_additional.branch_code) a
            ON kodeprod = a.kodeservice
            Where branch_code = branch
            GROUP BY komisi_trp, namaprod, kodeprod, jumlah";
		
		$query = $this->db->query($sql);

        //print_r($this->db->last_query()); die();

        return $query->result();
    }

	public function get_total_sesi_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal){
		$sql="Select b.kodekar, b.kodetrp, b.namakar,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT) FROM registrydet
				WHERE kodeservice='SUI'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND  kodekar='" . $kodekar ."') AS total_suite,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
				WHERE kodeservice='PRE'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND kodekar='" . $kodekar ."') AS total_pre,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
				WHERE kodeservice='PRS'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND  kodekar='" . $kodekar ."') AS total_prs,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
				WHERE kodeservice='STD'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND kodekar='" . $kodekar ."') AS total_std,
			(SELECT CAST(SUM(jml) AS INT) FROM registrydet
				WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS')  AND status='Y' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."'  AND kodekar='" . $kodekar ."') as total_sesi,
			(SELECT CAST(SUM(jmlext) AS INT) FROM registrydet
				WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS')  AND status='Y' AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'  AND kodekar='" . $kodekar ."') AS total_point
			From registrydet a
			JOIN tkar b ON a.kodekar = b.kodekar
			AND tgl>='" . $dari_tanggal ."'
			AND tgl<='" . $sampai_tanggal."'
			AND b.kodekar='" . $kodekar ."'
			AND a.status='Y'
			Group by b.[kodekar], b.kodetrp, b.namakar";
		
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		
		return $query->row();		
	}

	public function get_total_session_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
	

		$sql="Select b.kodekar, b.namakar, a.branch_code, b.kodetrp,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT) FROM registrydet
				WHERE kodeservice='SUI'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND  kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_suite,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
				WHERE kodeservice='PRE'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_pre,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
				WHERE kodeservice='PRS'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND  kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_prs,
			(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
				WHERE kodeservice='STD'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_std,
			(SELECT CAST(SUM(jml) AS INT) FROM registrydet
				WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS')  AND status='Y' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."'  AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') as total_sesi,
			(SELECT CAST(SUM(jmlext) AS INT) FROM registrydet
				WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS')  AND status='Y' AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'  AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_point
			From registrydet a
			JOIN tkar b ON a.kodekar = b.kodekar
			AND tgl>='" . $dari_tanggal ."'
			AND tgl<='" . $sampai_tanggal ."'
			AND b.kodekar='" . $kodekar ."'
			AND a.branch_code='" . $branch_id ."'
			AND a.status='Y'
			Group by b.[kodekar], b.kodetrp, b.namakar, a.branch_code";
		
		/*
		$sql="Select b.kodekar,
					(SELECT CAST(SUM(jmlext) AS INT) FROM registrydet
						WHERE kodeservice='SUI'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
						AND noreg In (Select noreg From registry Where kodekar='" . $kodekar ."' AND tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal."' AND branch_code='" . $branch_id ."')) AS total_suite,
					(SELECT CAST(SUM(jmlext) AS INT)  FROM registrydet
						WHERE kodeservice='PRE'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
						AND noreg In (Select noreg From registry Where kodekar='" . $kodekar ."' AND tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal."' AND branch_code='" . $branch_id ."')) AS total_pre,
					(SELECT CAST(SUM(jmlext) AS INT)  FROM registrydet
						WHERE kodeservice='PRS'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
						AND noreg In (Select noreg From registry Where kodekar='" . $kodekar ."' AND tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal."' AND branch_code='" . $branch_id ."')) AS total_prs,
					(SELECT CAST(SUM(jmlext) AS INT)  FROM registrydet
						WHERE kodeservice='STD'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
						AND noreg In (Select noreg From registry Where kodekar='" . $kodekar ."' AND tanggal>='" . $dari_tanggal ."' AND tanggal<='" . $sampai_tanggal."' AND branch_code='" . $branch_id ."')) AS total_std,
					(SELECT CAST(SUM(jmlext) AS INT) FROM registrydet
						WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS') AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
						AND noreg In (Select noreg From registry Where kodekar='" . $kodekar ."' AND tanggal>='" . $dari_tanggal."'AND tanggal<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."')) AS total_point,                
					(SELECT CAST(SUM(jml) AS INT) FROM registrydet
						WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS') AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
						AND noreg In (Select noreg From registry Where kodekar='" . $kodekar ."' AND tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal."' AND branch_code='" . $branch_id ."')) as total_sesi
					From registrydet a
					JOIN tkar b ON a.kodekar = b.kodekar
					AND tgl>='" . $dari_tanggal ."'
					AND tgl<='" . $sampai_tanggal."'
					AND b.kodekar='" . $kodekar ."'
					AND a.branch_code='" . $branch_id ."'
					AND a.status='Y'
					Group by a.branch_code, b.kodekar";
		*/				
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		
		return $query->row();		
	}

	public function get_total_session_by_branch($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select b.csname,  b.cname, 
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT) FROM registrydet
			WHERE kodeservice='SUI'  AND status='Y' AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND  branch_code='" . $branch_id ."') AS total_suite,
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
			WHERE kodeservice='PRE'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND  branch_code='" . $branch_id ."') AS total_pre,
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
			WHERE kodeservice='PRS'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND  branch_code='" . $branch_id ."') AS total_prs,
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
			WHERE kodeservice='STD'  AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'  AND  branch_code='" . $branch_id ."') AS total_std,
		(SELECT CAST(SUM(jml) AS INT) FROM registrydet
			WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS') AND status='Y' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."'  AND  branch_code='" . $branch_id ."') as total_sesi,
		(SELECT CAST(SUM(jmlext) AS INT) FROM registrydet
			WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS') AND status='Y' AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'  AND  branch_code='" . $branch_id ."') AS total_point
		From registrydet a
		JOIN branch b ON a.branch_code = b.csname
		AND tgl>='" . $dari_tanggal ."'
		AND tgl<='" . $sampai_tanggal ."'
		AND b.csname='" . $branch_id ."'
		AND a.status='Y'
		Group by b.csname, b.cname";
		
		$query = $this->db->query($sql);
		
		return $query->row();		
	}

	public function get_total_session_kar_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal){
		/*
		$sql="Select b.kodekar, b.namakar, 
			(SELECT COUNT(DISTINCT(nostock)) FROM sesi_therapist WHERE kodeservice='SUI' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."') AS total_suite,
			(SELECT COUNT(DISTINCT(nostock)) FROM sesi_therapist WHERE kodeservice='PRE' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."') AS total_pre,
			(SELECT COUNT(DISTINCT(nostock)) FROM sesi_therapist WHERE kodeservice='PRS' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."') AS total_prs,
			(SELECT SUM(jmlext) FROM registrydet WHERE kodeservice IN ('STD','PRE', 'SUI', 'PRS') AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."') AS total_point,                
			SUM(a.jmlsesi) as total_sesi
			From sesi_therapist a
			JOIN tkar b ON a.kodekar = b.kodekar
			AND tgl>='" . $dari_tanggal ."'
			AND tgl<='" . $sampai_tanggal ."'
			AND b.kodekar='" . $kodekar ."'
			Group by b.[kodekar], b.namakar";
			*/
		
		$sql="Select kodekar, namakar, 
			(SELECT COUNT(DISTINCT(noreg)) FROM registrydet WHERE kodeservice='SUI' AND kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."') AS total_suite,
			(SELECT COUNT(DISTINCT(noreg)) FROM registrydet WHERE kodeservice='PRE' AND kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."') AS total_pre,
			(SELECT COUNT(DISTINCT(noreg)) FROM registrydet WHERE kodeservice='PRS' AND kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."') AS total_prs,
			(SELECT SUM(jmlext) FROM registrydet WHERE kodeservice IN ('STD','PRE', 'SUI', 'PRS') AND kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."') AS total_point,                
			(SELECT SUM(jml) FROM registrydet WHERE kodeservice IN ('STD','PRE', 'SUI', 'PRS') AND kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."') AS total_sesi          
			From tkar
			Where kodekar='" . $kodekar ."'";
			
		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}		
	}

	public function get_detail_komisi_fnb($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="SELECT  sum(harga*jmlp) as total, sum(jmlp) as jumlah, harga, namaservice, kodeservice, sum(totalcigarette) as total_cigarette
			FROM registry_fnb
			Where kodegro='" . $kodekar ."'
			AND kodecateg!='003'
			AND tanggal>= '" . $dari_tanggal ."'
			AND tanggal<= '" . $sampai_tanggal ."'
			AND branch_code = '" . $branch_id ."'
			GROUP BY  kodeservice, namaservice, harga";

		$query = $this->db->query($sql);

		//print_r($this->db->last_query());
		
		return $query->result();
		
	}
	
	public function get_detail_komisi_lulur($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id = false){
	
		$sql="Select branch_name, tgl, nokamar, kodeservice, SUM(jml) as jmlsesi, SUM(jmlext) as jmlext
			FROM registrydet
			WHERE kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal . "'AND status = 'Y'
			GROUP BY noreg, nokamar, tgl, kodeservice, branch_name
			Order By tgl ASC";
		/*
		$sql="Select branch_name, tgl, nokamar, kodeservice, CAST(SUM(jml) AS INT) as jmlsesi, CAST(SUM(jmlext) AS INT) as jmlext
				From registrydet
				Where noreg IN (Select noreg From registry
								WHERE kodekar='" . $kodekar ."' AND tanggal >='" . $dari_tanggal . "'AND tanggal <='" . $sampai_tanggal . "' AND branch_code='" . $branch_id ."')
				AND branch_code='" . $branch_id ."'
				AND kodeservice IN ('SUI','PRE','PRS','STD')
				AND status='Y'
				GROUP BY noreg, nokamar, tgl, kodeservice, branch_name";
				Select b.kodekar, b.namakar, a.branch_code, b.kodetrp,
			(SELECT COUNT(DISTINCT(nostock)) FROM sesi_therapist WHERE kodeservice='SUI' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."' AND branch_code=a.branch_code) AS total_suite,
			(SELECT COUNT(DISTINCT(nostock)) FROM sesi_therapist WHERE kodeservice='PRE' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."' AND branch_code=a.branch_code) AS total_pre,
			(SELECT COUNT(DISTINCT(nostock)) FROM sesi_therapist WHERE kodeservice='PRS' AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code=a.branch_code) AS total_prs,
			(SELECT SUM(jmlext) FROM registrydet WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS') AND kodekar=b.kodekar AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."' AND branch_code=a.branch_code) AS total_point,                
			SUM(a.jmlsesi) as total_sesi
			From sesi_therapist a
			JOIN tkar b ON a.kodekar = b.kodekar
			AND tgl>='" . $dari_tanggal ."'
			AND tgl<='" . $sampai_tanggal ."'
			AND b.kodekar='" . $kodekar ."'
			AND a.branch_code='" . $branch_id ."'
			AND a.status='Y'
			Group by b.[kodekar], b.kodetrp, b.namakar, a.branch_code"
		*/
		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
		return $query->result();
		
	}

	public function get_detail_komisi_lulur_category($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select branch_name, kodeservice, SUM(jml) as jmlsesi, SUM(jmlext) as jmlext
			FROM registrydet
			WHERE kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal . "'AND branch_code ='" . $branch_id ."'
			AND kodeservice IN ('PRE','STD','SUI','PRS')
			GROUP BY kodeservice, branch_name";

		$query = $this->db->query($sql);
		return $query->result();
		
	}

	public function get_detail_komisi_lulur_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal){
		$sql="Select tgl, nokamar, kodeservice, SUM(jml) as jmlsesi, SUM(jmlext) as jmlext
			FROM registrydet
			WHERE kodekar='" . $kodekar ."' AND tgl>='" . $dari_tanggal . "'AND tgl<='" . $sampai_tanggal ."' AND status = 'Y'
			GROUP BY noreg, nokamar, tgl, kodeservice
			Order By tgl ASC";

			$query = $this->db->query($sql);

		return $query->result();
		
	}

	public function get_data_gaji_spv_therapist($dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="Select b.id_gaji, a.id_spv, a.nama_spv, a.is_mess, b.gaji, b.kuesioner, b.bpjs_prs, b.bpjs_kry, b.total_gaji
			FROM spv_therapist a
			JOIN gaji_spv_therapist b
			ON b.id_spv = a.id_spv
			WHERE b.tanggal>='" . $dari_tanggal . "' AND b.tanggal<='" . $sampai_tanggal ."'
			AND a.branch_code='" . $branch_id ."'
			GROUP BY a.id_spv, a.nama_spv, a.is_mess, b.gaji, b.kuesioner, b.total_gaji, b.bpjs_prs, b.bpjs_kry, b.id_gaji
			Order BY is_mess";

			$query = $this->db->query($sql);
			//print_r($this->db->last_query()); die();

		return $query->result();
	}

	public function get_data_gaji_spv_therapist_periode_by_id($id_gaji, $dari_tanggal, $sampai_tanggal){
		$sql="Select a.id_spv, a.nama_spv, a.is_mess, b.id_gaji, b.gaji, b.kuesioner, b.tunj_transport, b.tunj_luarkota, b.bpjs_kry, b.bpjs_prs, b.total_gaji
			FROM spv_therapist a
			JOIN gaji_spv_therapist b
			ON b.id_spv = a.id_spv
			WHERE b.tanggal>='" . $dari_tanggal . "' AND b.tanggal<='" . $sampai_tanggal ."'
			AND b.id_gaji='" . $id_gaji . "'";

			$query = $this->db->query($sql);
			
		return $query->row();
	}

	public function get_data_potongan_spv_therapist_periode_by_id($id_spv, $dari_tanggal, $sampai_tanggal){
		$sql="Select a.id_spv, a.nama_spv, a.is_mess, b.id_gaji, b.gaji, b.kuesioner, b.tunj_transport, b.tunj_luarkota, b.bpjs_kry, b.bpjs_prs, b.total_gaji
			FROM spv_therapist a
			JOIN gaji_spv_therapist b
			ON b.id_spv = a.id_spv
			WHERE b.tanggal>='" . $dari_tanggal . "' AND b.tanggal<='" . $sampai_tanggal ."'
			AND b.id_gaji='" . $id_gaji . "'";

			$query = $this->db->query($sql);
			
		return $query->row();
	}

	public function get_data_spv_therapist($branch_id){
		$sql="Select *
			FROM spv_therapist
			WHERE branch_code='" . $branch_id ."'
			Order BY id_spv";

			$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_detail_komisi_additional_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal){
				$sql="Select a.tanggal, a.namaservice, a.jml, a.branch_name,b.komisi_trp
				FROM point_additional a
				LEFT JOIN add_product b ON b.kodeprod=a.kodeservice
				WHERE a.kodekar='" . $kodekar ."' AND a.tanggal>='" . $dari_tanggal ."'AND a.tanggal<='" . $sampai_tanggal ."'
				AND a.kodeservice IN (Select kodeprod From add_product)
				Order By a.tanggal, a.noreg ASC";

		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
		return $query->result();
		
	}

	public function get_total_additional_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="SELECT SUM(jumlah*komisi_trp) as total_komisi_additional
			FROM add_product
			INNER JOIN (
				SELECT  point_additional.kodeservice, SUM(point_additional.jml) as jumlah, point_additional.branch_code as branch
			FROM point_additional
			WHERE point_additional.kodekar = '$kodekar'
			AND point_additional.tgl >= '$dari_tanggal'
			AND point_additional.tgl <= '$sampai_tanggal'
			AND point_additional.branch_code = '$branch_id'
			GROUP BY point_additional.kodeservice, point_additional.branch_code) a
			ON kodeprod = a.kodeservice
			Where branch_code = '$branch_id'";
			
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		if($query->num_rows() > 0 ){
			return $query->row();
		}else{
			return false;
		}
	}

	public function get_total_additional_kar_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal){
		$sql="SELECT DISTINCT SUM(jumlah*komisi_trp) as total_komisi_additional
				FROM add_product
				INNER JOIN (
					SELECT  point_additional.kodeservice, SUM(point_additional.jml) as jumlah, point_additional.branch_code as branch
					FROM point_additional
					WHERE point_additional.kodekar = '" . $kodekar ."'
					AND point_additional.tgl >= '" . $dari_tanggal ."'
					AND point_additional.tgl <= '" . $sampai_tanggal ."'
					GROUP BY point_additional.kodeservice, point_additional.branch_code) a
					ON kodeprod = a.kodeservice
					AND branch_code = a.branch";
			
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		if($query->num_rows() > 0 ){
			return $query->row();
		}else{
			return false;
		}
	}

	public function get_premium_room_by_kodekar ($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id){

       $sql="SELECT kodeservice, namaservice, COUNT(distinct registrydet.noreg) as jumlah, COUNT(distinct registrydet.noreg)*10000 as total_komisi, 10000 as harga_komisi
					FROM registrydet
					WHERE kodeservice IN ('PRE', 'PRS')
					AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'
					AND branch_code='" . $branch_id ."'
					AND kodekar='" . $kodekar ."'
					AND status = 'Y'
					GROUP BY kodeservice, namaservice";
	   
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		return $query->result();
	}
	
	public function get_premium_room_kar_by_kodekar ($dari_tanggal, $sampai_tanggal, $kodekar){

       $sql="SELECT kodeservice, namaservice, COUNT(distinct registrydet.noreg) as jumlah, COUNT(distinct registrydet.noreg)*10000 as total_komisi, 10000 as harga_komisi
					FROM registrydet
					WHERE kodeservice IN ('PRE', 'PRS')
					AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'
					AND kodekar='" . $kodekar ."'
					AND status = 'Y'
					GROUP BY kodeservice, namaservice";
	   
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		return $query->result();
	}
	
	public function get_total_komisi_premium_room($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id){

        $sql="SELECT kodeservice, namaservice, COUNT(distinct registrydet.noreg) as jumlah, COUNT(distinct registrydet.noreg)*10000 as total_komisi, 10000 as harga_komisi
			FROM registrydet
			WHERE kodeservice IN ('PRE', 'PRS')
			AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'
			AND branch_code='" . $branch_id ."'
			AND kodekar='" . $kodekar ."'
			AND status = 'Y'
			GROUP BY kodeservice, namaservice";
			
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();

		if($query->num_rows() > 0){
			return $query->row()->total_komisi;
		}else{
			return 0;
		}
    }

	public function get_potongan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
		$sql="SELECT *
			FROM potongan_gaji
			WHERE kodekar = '" . $kodekar ."'
			AND branch_code = '" . $branch_id ."'
			AND tanggal >= '" . $dari_tanggal . "'
			AND tanggal<= '" . $sampai_tanggal . "'";
			
		$query = $this->db->query($sql);
		 
		//print_r($this->db->last_query()); die();

		return $query->row();
		
	}

	public function get_potongan_kar_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal){
		$sql="SELECT TOP 1 *
			FROM potongan_gaji
			WHERE kodekar = '" . $kodekar ."'
			AND tanggal >= '" . $dari_tanggal ."'
			AND tanggal<= '" . $sampai_tanggal ."'
			ORDER BY yayasan DESC";
			
		$query = $this->db->query($sql);

		return $query->row();
		
	}

	public function save_potongan_by_kodekar($kodekar, $yayasan, $id_potongan, $branch_id){
		$data = array(
			'yayasan' => $yayasan
		 );
			
		$this->db->where('id_potongan >=', $id_potongan);
		$this->db->update('potongan_gaji', $data); 
		
	}

	public function update_komisi_fnb_by_id($id_fnb, $jumlah_komisi){
		$data = array(
			'jumlah_komisi' => $jumlah_komisi
		);
			
		$this->db->where('id_fnb', $id_fnb);
		$this->db->update('komisi_fnb', $data); 
		
	}

	public function reset_tabungan_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id){
		$data = array(
			'setoran' => '0'
		);
			
		$this->db->where('kodekar', $kodekar);
		$this->db->where('branch_code', $branch_id);		
		$this->db->where("tanggal >= '" . $dari_tanggal ."'");		
		$this->db->where("tanggal <= '" . $sampai_tanggal ."'");		
		$this->db->update('tabungan', $data); 
		
	}

	public function update_insert_spv_therapist($data){
		
		$id_spv = $data['id_spv'];
		
		unset($data['id_spv']);

		if($id_spv==null){
			$result = $this->db->insert('spv_therapist', $data); 
			
			if($result==true){
				return true;
			}else{
				return false;
			}	
		}else{
			$this->db->where('id_spv', $id_spv);
			$this->db->update('spv_therapist', $data); 
		}
	}
			
	public function update_gaji_spv_therapist($data){
		
		$id_gaji = $data['id_gaji'];

		unset($data['id_gaji']);

		$this->db->where('id_gaji', $id_gaji);
		$this->db->update('gaji_spv_therapist', $data); 

	}

	public function hitung_gaji_spv_therapist($dari_tanggal, $sampai_tanggal, $branch_id){

		$sql = "SELECT *
				FROM spv_therapist
				WHERE branch_code = '" . $branch_id ."'
				AND status = 1";
		
		$query = $this->db->query($sql);	
		$spv_trp = $query->result();	
		
		$query = $this->db->query($sql);	
		$spv_trp = $query->result();	
		
		foreach ($spv_trp as $trp){

			$sql_gaji = "SELECT *
				FROM gaji_spv_therapist
				WHERE branch_code = '" . $branch_id ."'
				AND tanggal >= '" . $dari_tanggal ."'
				AND tanggal <= '" . $sampai_tanggal ."'
				AND id_spv = '" . $trp->id_spv ."'";
			
			$query = $this->db->query($sql_gaji);	
			$gaji_spv = $query->num_rows();

			if($gaji_spv == 0){

				$data = array(
					'tanggal' => $dari_tanggal,
					'branch_code' => $branch_id,
					'id_spv' => $trp->id_spv,
					'nama_spv' => $trp->nama_spv,
				);
				
				$result = $this->db->insert('gaji_spv_therapist', $data);

				if($result==true){
					return 'Berhasil Di Masukkan';
				}else{
					return 'Gagal Di Masukkan';
				}
			}
		}		
	}

	public function update_komisi_lain_by_id($id_lain, $jumlah_komisi){
		$data = array(
			'jumlah_komisi' => $jumlah_komisi
		 );
			
		$this->db->where('id_lain', $id_lain);
		$this->db->update('komisi_lain', $data); 
		
	}
	public function update_komisi_kodes_by_id($id_lain, $jumlah_komisi){
		$data = array(
			'komisi_kodes' => $jumlah_komisi
		 );
			
		$this->db->where('id_lain', $id_lain);
		$this->db->update('komisi_lain', $data); 
		
	}

	public function update_komisi_kuesioner_by_id($id_kuesioner, $jumlah_komisi){
		$data = array(
			'jumlah_komisi' => $jumlah_komisi
		 );
			
		$this->db->where('id_kuesioner', $id_kuesioner);
		$this->db->update('komisi_kuesioner', $data); 
		
	}
	
	public function update_potongan_therapist_by_id($id_potongan, $data){
		$this->db->where('id_potongan', $id_potongan);
		$this->db->update('potongan_gaji', $data); 
	}

	public function update_potongan_by_column($column, $editval, $id_potongan){

		$this->db->set($column, $editval, false);
		$this->db->where('id_potongan', $id_potongan);
		$this->db->update('potongan_gaji'); 

		print_r($this->db->last_query()); die();
	
	}

	public function update_user($data, $user_id){

		if($data['password']==''){
			unset($data['password']);
		}else{
			$data['password'] = md5($data['password']);
		}
		unset($data['flag']);

		$this->db->where('user_id', $user_id);
		$result = $this->db->update('users', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_user($data){

		unset($data['flag']);

		$data['password'] = md5($data['password']);

		$data['created_at'] = date('Y-m-d H:i:s');

		$result = $this->db->insert('users', $data); 

		if($result==true){
			return true;
		}else{
			return false;
		}	
	}
	
	public function delete_user($user_id){

		$this->db->where('user_id', $user_id);
		$this->db->delete('users');

		return true;
	}
	
	public function delete_penerimaan($id){

		$this->db->where('receiving_id', $id);
		$this->db->delete('receiving');

		return true;
	}
	
	public function delete_pengiriman($id){

		$this->db->where('shipment_id', $id);
		$this->db->delete('shipment');

		return true;
	}

	function isUserExist($username) {
		$this->db->select('id_user');
		$this->db->where('username', $username);
		$query = $this->db->get('users_acc');
	
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}	

	public function get_branch_name_by_branch_id($branch_id){
        
        $this->db->select('name'); 
		$this->db->from('branch');
        $this->db->where('branch_id', $branch_id);
        $query = $this->db->get();
        return $query->row('name');
	}
	
	public function get_spv_name_by_id($id_spv){
        
        $this->db->select('nama_spv'); 
		$this->db->from('spv_therapist');
        $this->db->where('id_spv', $id_spv);
        $query = $this->db->get();
        return $query->row('nama_spv');
	}

	public function get_supplier_name_by_id($supplier_id){
        
        $this->db->select('name'); 
		$this->db->from('supplier');
        $this->db->where('supplier_id', $supplier_id);
        $query = $this->db->get();
        return $query->row('name');
	}
	
	public function get_tkar_by_kodetrp($kodetrp){
        
        $this->db->select('*'); 
		$this->db->from('tkar');
        $this->db->where('kodetrp', $kodetrp);
		$query = $this->db->get();
        return $query->row();
	}
	
	public function get_kodetrp_by_kodekar($kodekar, $branch_id=false){
        $this->db->select('kodetrp'); 
		$this->db->from('tkar');
        $this->db->where('kodekar', $kodekar);
        $query = $this->db->get();
        return $query->row('kodetrp');
	}

	public function get_kodetrp_by_kodekar_makasar($kodekar){
        $this->db->select('kodetrp'); 
		$this->db->from('tkar');
        $this->db->where('kodekar', $kodekar);
        $query = $this->db->get();
        return $query->row('kodetrp');
	}
	
	public function get_level_komisi_by_kodekar($kodekar){
        
        $this->db->select('levelkomisi'); 
		$this->db->from('tkar');
        $this->db->where('kodekar', $kodekar);
        $query = $this->db->get();
        return $query->row('levelkomisi');
	}
	
	public function getAddProdByKodeKar($kodekar, $dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("kodeservice, COUNT(*) as jumlah"); 		
		$this->db->from("point_additional");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$this->db->group_by("kodeservice");
		$this->db->order_by("kodeservice ASC");
		
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getTotalSesiTherapist($kodekar, $branch_id) {
	
		$dari_tanggal = (date('d') <= 25) ? date('Y-m-d', strtotime(date('Y-m-21') . '- 1 month')) : date('Y-m-21');
		$sampai_tanggal = date('Y-m-d');
		
		$sql="Select b.kodekar, b.namakar, a.branch_code,
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT) FROM registrydet
		WHERE kodeservice='SUI'  AND status='Y' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."' AND  kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_suite,
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
			WHERE kodeservice='PRE'  AND status='Y' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."' AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_pre,
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
			WHERE kodeservice='PRS'  AND status='Y'  AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."' AND  kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_prs,
		(SELECT CAST(COUNT(DISTINCT noreg) AS INT)  FROM registrydet
			WHERE kodeservice='STD'  AND status='Y'  AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."' AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_std,
		(SELECT CAST(SUM(jml) AS INT) FROM registrydet
			WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS')   AND status='Y' AND tgl>='" . $dari_tanggal ."' AND tgl<='" . $sampai_tanggal ."'  AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') as total_sesi,
		(SELECT CAST(SUM(jmlext) AS INT) FROM registrydet
			WHERE kodeservice IN ('STD', 'PRE', 'SUI', 'PRS')   AND status='Y'  AND tgl>='" . $dari_tanggal ."'AND tgl<='" . $sampai_tanggal ."'  AND kodekar='" . $kodekar ."' AND  branch_code='" . $branch_id ."') AS total_point
		From registrydet a
		JOIN tkar b ON a.kodekar = b.kodekar
		AND tgl>='" . $dari_tanggal ."'
		AND tgl<='" . $sampai_tanggal ."'
		AND a.kodekar='" . $kodekar ."'
		AND a.status = 'Y'
		Group by b.[kodekar], b.namakar, a.branch_code";
		
		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	
	}

	public function get_total_add_therapist($kodekar) {
		$dari_tanggal = (date('d') <= 25) ? date('Y-m-d', strtotime(date('Y-m-21') . '- 1 month')) : date('Y-m-21');
		$date_now = date('Y-m-d');

		/*
		$sql="SELECT point_additional.kodekar, SUM(add_product.komisi_trp) as total_komisi_additional
			FROM point_additional
			LEFT JOIN add_product ON add_product.kodeprod = point_additional.kodeservice
			WHERE point_additional.kodekar = '" . $kodekar ."'
			AND point_additional.tgl >= '" . $dari_tanggal . "'
			AND point_additional.tgl <= '" . $date_now . "'
			GROUP BY point_additional.kodekar";
		*/

		$sql="SELECT SUM(jumlah*komisi_trp) as total_komisi_additional
			FROM add_product
			INNER JOIN (
				SELECT  point_additional.kodeservice, SUM(point_additional.jml) as jumlah, point_additional.branch_code as branch
			FROM point_additional
			WHERE point_additional.kodekar = '$kodekar'
			AND point_additional.tgl >= '$dari_tanggal'
			AND point_additional.tgl <= '$date_now'
			GROUP BY point_additional.kodeservice, point_additional.branch_code) a
			ON kodeprod = a.kodeservice
			Where branch_code = branch";
			
		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		if($query->num_rows() > 0 ){
			return $query->row()->total_komisi_additional;
		}else{
			return false;
		}
	
	}	

	public function getTotalKomisiLulur($kodekar) {
		$dari_tanggal = (date('d') <= 25) ? date('Y-m-d', strtotime(date('Y-m-21') . '- 1 month')) : date('Y-m-21');
		$date_now = date('Y-m-d');
		
		$sql="SELECT point_additional.kodekar, SUM(add_product.komisi_trp) as total_komisi_additional
			FROM point_additional
			LEFT JOIN add_product ON add_product.kodeprod = point_additional.kodeservice
			WHERE point_additional.kodekar = '" . $kodekar ."'
			AND point_additional.tgl >= '" . $dari_tanggal . "'
			AND point_additional.tgl <= '" . $date_now . "'
			GROUP BY point_additional.kodekar";
			
		$query = $this->db->query($sql);

		$sql="Select * 
			FROM registry
			WHERE kodekar='" . $kodekar ."' AND tanggal>='" . $dari_tanggal ."'AND tanggal<='" . $sampai_tanggal ."' AND branch_code='" . $branch_id ."'
			Order By tanggal ASC";

		$query = $this->db->query($sql);

		return $query->result();

		//print_r($this->db->last_query()); die();

		return $query->row()->total_komisi_additional;
	}

	public function get_komisi_fnb_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id) {
		$sql="SELECT jumlah_komisi
			FROM komisi_fnb
			WHERE kodekar = '" . $kodekar ."'
			AND branch_code = '" . $branch_id ."'
			AND tanggal >= '" . $dari_tanggal . "'
			AND tanggal<= '" . $sampai_tanggal . "'";
			
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	}	

	public function get_komisi_fnb_kar_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id) {
		$sql="SELECT SUM(CAST(jumlah_komisi as float)) as jumlah_komisi
			FROM komisi_fnb
			WHERE kodekar = '" . $kodekar ."'
			AND tanggal >= '" . $dari_tanggal . "'
			AND tanggal<= '" . $sampai_tanggal . "'";
			
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return 0;
		}
	}	

	public function get_komisi_lain_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id) {
		$sql="SELECT jumlah_komisi
			FROM komisi_lain
			WHERE kodekar = '" . $kodekar ."'
			AND tanggal >= '" . $dari_tanggal . "'
			AND branch_code >= '" . $branch_id . "'
			AND tanggal<= '" . $sampai_tanggal . "'";
			
		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->row('jumlah_komisi');
		}else{
			return 0;
		}
	}

	public function get_komisi_fnb($dari_tanggal, $sampai_tanggal, $kodekar) {
		$sql="SELECT jumlah_komisi
			FROM komisi_fnb
			WHERE kodekar = '" . $kodekar ."'
			AND branch_code = '" . $branch_id . "'
			AND tanggal >= '" . $dari_tanggal . "'
			AND tanggal<= '" . $sampai_tanggal . "'";
			
		$query = $this->db->query($sql);

		return $query->row();
	}

	public function get_komisi_kuesioner_by_kodekar($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id) {
		$sql="SELECT jumlah_komisi
			FROM komisi_kuesioner
			WHERE kodekar = '" . $kodekar ."'
			AND branch_code = '" . $branch_id . "'
			AND tanggal >= '" . $dari_tanggal . "'
			AND tanggal<= '" . $sampai_tanggal . "'";
			
		$query = $this->db->query($sql);

		$data = $query->row('jumlah_komisi');

		if($data==null){
			$data = 0;
		}else{
			$data = $data;
		}

		//print_r($this->db->last_query()); die();

		return $data;
	}

	public function get_komisi_kodes_by_kodekar($kodekar, $dari_tanggal, $sampai_tanggal, $branch_id) {
		$sql="SELECT komisi_kodes
			FROM komisi_lain
			WHERE kodekar = '" . $kodekar ."'
			AND branch_code = '" .$branch_id . "'
			AND tanggal >= '" . $dari_tanggal . "'
			AND tanggal<= '" . $sampai_tanggal . "'";
			
		$query = $this->db->query($sql);

		$data = $query->row('komisi_kodes');

		if($data==null){
			$data = 0;
		}else{
			$data = $data;
		}

		//print_r($this->db->last_query());

		return $data;
	}

	public function get_total_komisi_fnb($dari_tanggal, $sampai_tanggal, $kodekar){
		
		$sql="SELECT  kodegro, namagro, sum(totalcigarette) as total_cigarette, sum(harga*jmlp) as total
			FROM registry_fnb
			Where kodegro='$kodekar'
			AND kodecateg !='003'
			AND tanggal>='$dari_tanggal' AND tanggal<='$sampai_tanggal'
			GROUP BY  kodegro, namagro
			ORDER BY  kodegro ASC";

		$query = $this->db->query($sql);
		//print_r($this->db->last_query()); die();
		return $query->row('total');
		
	}

	public function get_view_komisi_fnb($dari_tanggal, $sampai_tanggal, $kodekar, $branch_id){
		
		$sql="SELECT  sum(harga*jmlp) as total, harga, sum(jmlp) as jumlah, namaservice, kodeservice, sum(totalcigarette) as total_cigarette
			FROM registry_fnb
			Where kodegro='" . $kodekar . "'
			AND kodecateg!='003'
			AND tanggal>= '" . $dari_tanggal . "'
			AND tanggal<= '" . $sampai_tanggal . "'
			AND branch_code= '" . $branch_id . "'
			GROUP BY  kodeservice, namaservice, harga";

		$query = $this->db->query($sql);

		//print_r($this->db->last_query()); die();
		
		return $query->result();
	}

	public function get_view_kar_komisi_fnb($dari_tanggal, $sampai_tanggal, $kodekar){
		
		$sql="SELECT  sum(harga*jmlp) as total, harga, sum(jmlp) as jumlah, namaservice, kodeservice, sum(totalcigarette) as total_cigarette
			FROM registry_fnb
			Where kodegro='$kodekar'
			AND kodecateg!='003'
			AND tanggal>= '$dari_tanggal'
			AND tanggal<='$sampai_tanggal'
			GROUP BY  kodeservice, namaservice, harga";

		$query = $this->db->query($sql);
		
		//print_r($this->db->last_query()); die();
		return $query->result();
	}

	public function get_komisi_fnb_therapist($dari_tanggal, $sampai_tanggal, $branch_id){
		
		$sql="SELECT a.id_fnb, a.kodekar, jumlah_komisi, a.tanggal, b.kodetrp, b.levelkomisi, b.namakar
				FROM komisi_fnb a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";
		$query = $this->db->query($sql);

		/*
		if($query->num_rows() == 0){
			
			$sql_trp="SELECT namakar
					FROM registry a
					LEFT JOIN tkar b ON b.kodekar = a.kodekar
					WHERE b.therapist = 1
					AND a.branch_code = '$branch_id'
					AND a.tanggal >= '$dari_tanggal'
					AND a.tanggal <= '$sampai_tanggal'			
					ORDER BY b.kodekar ASC";
			$query = $this->db->query($sql_trp);

			$data_trp = $query->row('namakar');

				$sql_komisi ="SELECT  kodegro, namagro, sum(totalcigarette) as total_cigarette, sum(harga*jmlp)-sum(totalcigarette) as total
							FROM rpt_therapist.[dbo].registry_fnb
							Where namagro LIKE '%$data_trp'
							AND tanggal>='$dari_tanggal' AND tanggal <= '$sampai_tanggal'
							GROUP BY  kodegro, namagro
							ORDER BY  kodegro ASC";

				$query = $this->db->query($sql_komisi);

			$komisi_fnb = $query->row('total');
			
			if($komisi_fnb==null){
				$komisi_fnb = 0;
			}else{
				$komisi_fnb = $komisi_fnb;
			}
			//print_r($this->db->last_query()); die();

			$sql="INSERT INTO komisi_fnb (kodekar, tanggal, branch_code, jumlah_komisi)
				Select DISTINCT a.kodekar, MAX(a.tgl) as tgl, branch_code, $komisi_fnb
				From sesi_therapist a
				JOIN tkar b ON b.kodekar = a.kodekar
				Where branch_code = '$branch_id'
				AND a.tgl  >= '$dari_tanggal'
				AND a.tgl <= '$sampai_tanggal'
				GROUP BY a.kodekar, branch_code
				ORDER BY kodekar ASC";
			
			$this->db->query($sql);

			$sql="SELECT a.id_fnb, a.kodekar, jumlah_komisi, b.kodetrp, b.levelkomisi, b.namakar
				FROM komisi_fnb a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

				$query = $this->db->query($sql);
		}
		*/

		//print_r($this->db->last_query()); die();
		return $query->result();
	}

	public function get_komisi_lain($dari_tanggal, $sampai_tanggal, $branch_id){
		
		$sql="SELECT a.id_lain, a.kodekar, komisi_kodes, jumlah_komisi, a.tanggal, b.kodetrp, b.levelkomisi, b.namakar
				FROM komisi_lain a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

		$query = $this->db->query($sql);
		/*
		if($query->num_rows() == 0){
			$sql="INSERT INTO komisi_lain (kodekar, tanggal, branch_code, jumlah_komisi)
				Select DISTINCT a.kodekar, MAX(a.tgl) as tgl, a.branch_code, 0
				From sesi_therapist a
				JOIN tkar b ON b.kodekar = a.kodekar
				Where a.branch_code = '$branch_id'
				AND a.tgl  >= '$dari_tanggal'
				AND a.tgl <= '$sampai_tanggal'
				GROUP BY a.kodekar, a.branch_code
				ORDER BY kodekar ASC";
			
			$this->db->query($sql);

			$sql="SELECT a.id_lain, a.kodekar, jumlah_komisi, b.kodetrp, b.levelkomisi, b.namakar
				FROM komisi_lain a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

				$query = $this->db->query($sql);
			return $query->result();
		}
		*/
		return $query->result();
	}

	public function get_komisi_kuesioner($dari_tanggal, $sampai_tanggal, $branch_id){
		
		$sql="SELECT a.id_kuesioner, a.kodekar, jumlah_komisi, a.tanggal, b.kodetrp, b.levelkomisi, b.namakar
				FROM komisi_kuesioner a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1				
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

		$query = $this->db->query($sql);
		/*
		$sql="SELECT a.id_kuesioner, a.kodekar, jumlah_komisi, a.tanggal, b.kodetrp, b.levelkomisi, b.namakar
				FROM komisi_kuesioner a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1				
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

		$query = $this->db->query($sql);
		
		if($query->num_rows() == 0){

			$sql="INSERT INTO grade_therapist (kodekar, namakar, awal, akhir, total_tamu, point, branch_code, total_ts, ts_persen, ts_nilai, ratio_nilai, gc_comment, gc_comment_persen, gc_comment_nilai, hasil, grade)
				Select DISTINCT a.kodekar, b.namakar, MIN(a.tgl) as awal,
				MAX(a.tgl) as akhir, count(DISTINCT(nostock)) as total_tamu, sum(jmlext) as point, branch_code,
				(Select count(DISTINCT(nostock)) as total_tamu From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS') as total_ts,
				(SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) as request_persen,
				(Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 50 And 100 Then 25
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 30 And 49.99 Then 10
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 19 And 29.99 Then 5
				Else 0 End) as request_nilai,
				(Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 110 And 199 Then 10
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 100 And 109 Then 5
				Else 0 End) as ratio_nilai,
				(SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal'
							And kodekar = a.kodekar) a) as gc_comment,
				((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) as gc_comment_persen,
				(Case When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) >= 95 Then 15
					When((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 85 And 94 Then 10
					When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 75 And 84 Then 5
					Else 0 End) as gc_comment_nilai,
				(SELECT (Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 50 And 100 Then 25
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 30 And 49.99 Then 10
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 19 And 29.99 Then 5
				Else 0 End)+(Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 110 And 199 Then 10
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 100 And 109 Then 5
				Else 0 End)+(Case When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) >= 95 Then 15
					When((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 85 And 94 Then 10
					When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 75 And 84 Then 5
					Else 0 End)) as hasil,
				Case When (SELECT (Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 50 And 100 Then 25
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 30 And 49.99 Then 10
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 19 And 29.99 Then 5
				Else 0 End)+(Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 110 And 199 Then 10
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 100 And 109 Then 5
				Else 0 End)+(Case When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) >= 95 Then 15
					When((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 85 And 94 Then 10
					When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 75 And 84 Then 5
					Else 0 End)) Between 45 And 55 Then 'A'
				When (SELECT (Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 50 And 100 Then 25
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 30 And 49.99 Then 10
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 19 And 29.99 Then 5
				Else 0 End)+(Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 110 And 199 Then 10
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 100 And 109 Then 5
				Else 0 End)+(Case When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) >= 95 Then 15
					When((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 85 And 94 Then 10
					When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 75 And 84 Then 5
					Else 0 End)) Between 35 And 44 Then 'B'
				When (SELECT (Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 50 And 100 Then 25
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 30 And 49.99 Then 10
					When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal' AND statustrp='TS')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 19 And 29.99 Then 5
				Else 0 End)+(Case When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) >= 120 Then 15
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 110 And 199 Then 10
				When (SELECT ROUND(((CONVERT(decimal(10,2), (SELECT sum(jmlext) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))  / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal')))*100), 0)) Between 100 And 109 Then 5
				Else 0 End)+(Case When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) >= 95 Then 15
					When((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 85 And 94 Then 10
					When ((SELECT performance+massage+service
				From (Select Sum(Optbqpj) As performance,
							Sum(Optbqpn) As massage,
							Sum(Optbqs) As service
						From loggc
						Where CONVERT(Varchar(8), tanggal, 112) Between '$dari_tanggal' And '$sampai_tanggal' AND branch_code = '$branch_id'
							And kodekar = a.kodekar) a) / (SELECT CONVERT(decimal(10,2), (SELECT count(DISTINCT(nostock)) From logtherapist Where kodekar=a.kodekar AND branch_code = '$branch_id' AND tgl >= '$dari_tanggal' AND tgl <= '$sampai_tanggal'))*3) * 100) Between 75 And 84 Then 5
					Else 0 End)) Between 30 And 34 Then 'C'
				Else 'D' End As Grade
				From logtherapist a
				JOIN tkar b ON b.kodekar = a.kodekar
				Where branch_code = '$branch_id'
				AND a.tgl >= '$dari_tanggal'
				AND a.tgl <= '$sampai_tanggal'
				GROUP BY a.kodekar, b.namakar, branch_code
				ORDER BY kodekar ASC";
			
			$this->db->query($sql);
			
			$sql2="INSERT INTO komisi_kuesioner (kodekar, tanggal, branch_code, jumlah_komisi)
					Select DISTINCT a.kodekar, MAX(a.tgl) as tgl, branch_code,
					(Case When (Select grade from grade_therapist
								Where branch_code = '$branch_id' AND awal  >= '$dari_tanggal' AND akhir <= '$sampai_tanggal' AND kodekar=a.kodekar) = 'A' Then '500000'
							When(Select grade from grade_therapist
								Where branch_code = '$branch_id' AND awal  >= '$dari_tanggal' AND akhir <= '$sampai_tanggal' AND kodekar=a.kodekar) = 'B' Then '300000'
						Else '0' End) as nominal_komisi
					From sesi_therapist a JOIN tkar b ON b.kodekar = a.kodekar
					Where branch_code = '$branch_id' AND a.tgl >= '$dari_tanggal' AND a.tgl <= '$sampai_tanggal'
					GROUP BY a.kodekar, branch_code
					ORDER BY kodekar ASC";
			
			$this->db->query($sql2);	
			//print_r($this->db->last_query()); die();		

			$sql="SELECT a.id_kuesioner, a.kodekar, jumlah_komisi, a.tanggal, b.kodetrp, b.levelkomisi, b.namakar
				FROM komisi_kuesioner a
				LEFT JOIN tkar b ON b.kodekar = a.kodekar
				WHERE b.therapist = 1
				AND a.branch_code = '$branch_id'
				AND a.tanggal >= '$dari_tanggal'
				AND a.tanggal <= '$sampai_tanggal'			
				ORDER BY b.kodekar ASC";

			$query = $this->db->query($sql);
		}
		*/
		return $query->result();
	}

	public function get_komisi_fnb_by_id($id_fnb, $kodekar) {
		
		$this->db->select("*"); 		
		$this->db->from("komisi_fnb");
		$this->db->where("id_fnb",  $id_fnb);		
		$this->db->where("kodekar",  $kodekar);	
		
		$query = $this->db->get();
		//print_r($this->db->last_query()); die();
		return $query->row();		
	}

	public function get_spv_therapist_by_id($id_spv) {
		
		$this->db->select("*"); 		
		$this->db->from("spv_therapist");
		$this->db->where("id_spv",  $id_spv);	
		
		$query = $this->db->get();

		return $query->row();	
	}

	public function get_komisi_lain_by_id($id_lain, $kodekar) {
		
		$this->db->select("*"); 		
		$this->db->from("komisi_lain");
		$this->db->where("id_lain",  $id_lain);		
		$this->db->where("kodekar",  $kodekar);	
		
		$query = $this->db->get();
		//print_r($this->db->last_query()); die();
		return $query->row();
		
	}

	public function insert_notes($data) {

		$data['created'] = date('Y-m-d H:i:s');
		$data['users_id'] = $this->userdata->user_id_user;
		
		$insert = $this->db->insert('notes', $data);
			
		if($insert==true){
			return 'Catatan Berhasil Di Masukkan';
		}else{
			return 'Catatan Gagal Di Masukkan';
		}		
	}

	public function view_notes($dari_tanggal, $sampai_tanggal, $kodekar) {

		$this->db->select("*"); 		
		$this->db->from("notes");
		$this->db->where("kodekar",  $kodekar);		
		$this->db->where("awal",  $dari_tanggal);			
		$this->db->where("akhir",  $sampai_tanggal);			
		$query = $this->db->get();
		
		return $query->result();

	}

	public function get_count_notes($kodekar, $dari_tanggal, $sampai_tanggal){

		$this->db->select("*"); 		
		$this->db->from("notes");
		$this->db->where("kodekar",  $kodekar);		
		$this->db->where("awal",  $dari_tanggal);			
		$this->db->where("akhir",  $sampai_tanggal);			
		$query = $this->db->get();

		return $query->num_rows();	

	}

	public function get_komisi_kuesioner_by_id($id_kuesioner, $kodekar) {
		
		$this->db->select("*"); 		
		$this->db->from("komisi_kuesioner");
		$this->db->where("id_kuesioner",  $id_kuesioner);		
		$this->db->where("kodekar",  $kodekar);	
		
		$query = $this->db->get();
		//print_r($this->db->last_query()); die();
		return $query->row();
		
	}

	public function get_potongan_therapist_by_id($id_potongan, $kodekar) {
		
		$this->db->select("*"); 		
		$this->db->from("potongan_gaji");
		$this->db->where("id_potongan",  $id_potongan);		
		$this->db->where("kodekar",  $kodekar);	
		
		$query = $this->db->get();
		return $query->row();
		
	}
	
	public function getNetSales($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("SUM(Quantity*harga) as TotalNet"); 		
		$this->db->from("KasirDT");
		$this->db->where("Nomor IN (Select Nomor FROM KasirMS WHERE Tanggal >= '" . $dari_tanggal . "' AND Tanggal <= '" . $sampai_tanggal . "')");
		
		$query = $this->db->get();
		return $query->row("TotalNet");
	}
	
	public function getNumbOfTransaction($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("COUNT(*) as NumbTransaction"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");$query = $this->db->get();
		return $query->row("NumbTransaction");
	}
	
	public function getAvgSale($dari_tanggal, $sampai_tanggal) {
		
		$this->db->select("(SUM(Total) / COUNT(*)) as AvgSales"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$query = $this->db->get();
		
		return $query->row("AvgSales");
	}
	
	public function getSalesAverage($dari_tanggal, $sampai_tanggal, $limit,$start) {	
		$this->db->select("Tanggal"); 		
		$this->db->select("(COUNT(*)) as TotalBill"); 		
		$this->db->select("(SUM(Total) / COUNT(*)) as AvgSales"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$this->db->group_by("Tanggal", "ASC");
		$this->db->limit($limit, $start);	
		//$this->db->order_by($col,$dir);	
	
		$query = $this->db->get();
		
		//print_r($this->db->last_query());
		
		return $query->result();
	}
	
	public function getCountSalesAverage($dari_tanggal, $sampai_tanggal) {		
		
		$this->db->select("Tanggal"); 		
		$this->db->select("(COUNT(*)) as TotalBill"); 		
		$this->db->select("(SUM(Total) / COUNT(*)) as AvgSales"); 		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$this->db->group_by("Tanggal", "ASC");
		$query = $this->db->get();
		
		return $query->num_rows();  
	}
	
	public function getChartSales($dari_tanggal, $sampai_tanggal) {
		$this->db->select("CONVERT(varchar, Tanggal, 120) as Tanggal");	
		$this->db->select("SUM(Total) as TotalGross"); 
		$this->db->select("COUNT(*) as NumbTransaction");		
		$this->db->from("KasirMS");
		$this->db->where("Tanggal >= '" . $dari_tanggal . "'");
		$this->db->where("Tanggal <= '" . $sampai_tanggal . "'");
		$this->db->group_by("Tanggal", "ASC");
		$query = $this->db->get();
		
		return $query->result();
	}
	
	public function getChartSalesDaily($tanggal) {
		$this->db->select("Nomor"); 
		$this->db->select("CONVERT(varchar, Jam, 108) as Jam, Total");	
		$this->db->from("KasirMS");
		$this->db->where("Tanggal = '" . $tanggal . "'");
		$query = $this->db->get();
		
		return $query->result();
	}
	
	public function getTotaltransaksi() {
		$date_now = date('Y-m-d');
		$this->db->from("KasirMS");
		
		$query = $this->db->get();
		
        return $query->result();
	}
	
	public function getMonthlyTransaksi(){
		$year = date('Y');
		$month = date('m');
		$this->db->from("KasirMS");
		$this->db->where('MONTH(Tanggal) = ' .$month);
		$this->db->where('YEAR(Tanggal) = ' .$year);
		
		$query = $this->db->get();
		
		//print_r($this->db->last_query()); die();
		
        return $query->result();
	}
}

/* End of file M_pegawai.php */
/* Location: ./application/models/M_pegawai.php */