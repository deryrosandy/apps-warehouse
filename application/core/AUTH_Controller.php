<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AUTH_Controller extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_Auth');

		$this->userdata = $this->session->userdata('userdata');
		
		$this->session->set_flashdata('segment', explode('/', $this->uri->uri_string()));

		//var_dump($this->session->userdata('status')); die();

		if ($this->session->userdata('status') == '') {
			redirect('auth');
		}
	}
}

/* End of file MY_Auth.php */
/* Location: ./application/core/MY_Auth.php */