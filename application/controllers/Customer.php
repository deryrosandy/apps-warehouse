<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General'));
	}

	public function index() {

		$this->customer();
	}

	public function customer() {

		$data['userdata'] 		= $this->userdata;	

		$data['customers'] = $this->M_General->get_all_customer();

		$data['page'] 			= "customer";
		$data['judul'] 			= "Customer";
		$data['deskripsi'] 		= "";
		
		$this->template->views('customer/list_customer', $data);
	}

	public function tambah_customer() {
		
		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "customer";
		$data['judul'] 			= " ";
		$data['deskripsi'] 		= " ";
		
		$this->template->views('customer/tambah_customer', $data);
	}

	public function insert_customer() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$customer_id = $this->input->post('customer_id', TRUE);
		unset($data['customer_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($customer_id)) { 
					
					$result = $this->M_General->update_customer($data, $customer_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Customer Berhasil Di Update'));
						$url = base_url() . 'customer';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Customer Gagal Di Update'));
						$url = base_url() . 'customer';
						redirect($url);
						//header("Refresh:0");
					}

				}else{
					$result = $this->M_General->insert_customer($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Customer Berhasil Dimasukkan'));
						$url = base_url() . 'customer';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Customer Gagal Dimasukkan'));
						$url = base_url() . 'customer';
						redirect($url);
						//header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('customer/tambah_customer', $data);
	
	}

	public function update_customer($customer_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['customer'] = $this->M_General->get_customer_by_id($customer_id);

		$data['page'] 			= "customer";
		$data['judul'] 			= "Update Customer";
		$data['deskripsi'] 		= "";
		
		$this->template->views('customer/tambah_customer', $data);
	
	}

	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_customer($id);
	
		echo json_encode($result);
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */