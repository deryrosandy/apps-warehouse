<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class profile extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {

		$data['userdata'] 	= $this->userdata;
		$user_id = $data['userdata']->user_id;

		$data['page'] 			= "profile";
		$data['judul'] 			= "profile";
		$data['deskripsi'] 		= "";

		$data['profile'] = $this->M_General->get_user_by_id($user_id);
		
		$this->template->views('profile', $data);
	
	}

	public function update_profile() {
		
		$data = $this->input->post();
		
		$user_id = $this->input->post('user_id', TRUE);
		unset($data['user_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($user_id)) { 
					
					$result = $this->M_General->update_user($data, $user_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Profile Berhasil Di Update'));
						$url = base_url() . 'profile';
						redirect($url);
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Profile Gagal Di Update'));
						$url = base_url() . 'profile';
						redirect($url);
					}

				}
			}
		}
		
		$this->template->views('users/tambah_user', $data);
	
	}

	public function ganti_password() {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "ganti_password";
		$data['judul'] 			= "Ganti Password";
		$data['deskripsi'] 		= "";		

		$data_post = $this->input->post();
		
		$flag = $this->input->post('flag', TRUE);
	
		if ($flag == 'flag'){
			
			$result = $this->M_General->update_password($data_post);
			
			if ($result==true) {
				$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));				
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
			}
		}

		$this->template->views('ganti_password', $data);
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */