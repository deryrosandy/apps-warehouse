<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General'));
	}
	
	public function index() {
		$this->list_barang();
	}

	public function update_barang($product_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['product'] = $this->M_General->get_product_by_id($product_id);
		$data['categories'] = $this->M_General->get_all_product_category();

		$data['page'] 			= "update_barang";
		$data['judul'] 			= "Update Barang";
		$data['deskripsi'] 		= "";
		
		$this->template->views('barang/tambah_barang', $data);
	
	}

	public function list_barang() {
		
		$data['userdata'] 		= $this->userdata;	

		$data['products'] = $this->M_General->get_all_product();

		$data['page'] 			= "data_barang";
		$data['judul'] 			= "Data Barang";
		$data['deskripsi'] 		= "";
		
		$this->template->views('barang/list_barang', $data);
	}
	
	public function stock_barang() {
		
		$data['userdata'] 		= $this->userdata;	

		$data['products'] = $this->M_General->get_all_stock_product();
		

		$data['page'] 			= "stock_barang";
		$data['judul'] 			= "Stock Barang";
		$data['deskripsi'] 		= "";
		
		$this->template->views('barang/stock_barang', $data);
	}
	

	public function update_stock($product_id) {
		
		$data['userdata'] 		= $this->userdata;	

		$data['stock'] = $this->M_General->get_product_stock_by_id($product_id);
		$data['page'] 			= "stock_barang";
		$data['judul'] 			= "Stock Barang";
		$data['deskripsi'] 		= "";
		
		$this->template->views('barang/update_stock', $data);
	}
	
	public function insert_stock() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$product_stock_id = $this->input->post('product_stock_id', TRUE);
		unset($data['product_stock_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($product_stock_id)) { 
					
					$result = $this->M_General->update_product_stock($data, $product_stock_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Stock Barang Berhasil Di Update'));
						$url = base_url() . 'barang/stock_barang';
						redirect($url);
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Stock Barang Gagal Di Update'));
						$url = base_url() . 'barang/stock_barang';
						redirect($url);
					}

				}
			}
		}
		
		$this->template->views('barang/update_stock', $data);
	
	}

	public function tambah_barang() {
		
		$data['userdata'] 		= $this->userdata;	

		$data['categories'] = $this->M_General->get_all_product_category();

		$data['page'] 			= "data_barang";
		$data['judul'] 			= " ";
		$data['deskripsi'] 		= " ";
		
		$this->template->views('barang/tambah_barang', $data);
	}
	
	public function insert_barang() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$product_id = $this->input->post('product_id', TRUE);
		unset($data['product_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($product_id)) { 
					
					$result = $this->M_General->update_product($data, $product_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Barang Berhasil Di Update'));
						$url = base_url() . 'barang';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Barang Gagal Di Update'));
						$url = base_url() . 'barang';
						redirect($url);
						header("Refresh:0");
					}

				}else{
					$result = $this->M_General->insert_product($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Barang Berhasil Dimasukkan'));
						$url = base_url() . 'barang';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Barang Gagal Dimasukkan'));
						$url = base_url() . 'barang';
						redirect($url);
						header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('barang/tambah_barang', $data);
	
	}

	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_barang($id);
	
		echo json_encode($result);
	}
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */