<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {

		$data['userdata'] = $this->userdata;

		$data['page'] 			= "dashboard";
		$data['judul'] 			= "Dashboard";
		$data['deskripsi'] 		= "Anev Report System";

		$data['total_penerimaan'] = $this->M_General->get_all_penerimaan();
		$data['total_pengiriman'] = $this->M_General->get_all_pengiriman();
		$data['total_customer'] = $this->M_General->get_all_customer();
		$data['total_vendor'] = $this->M_General->get_all_vendor();
			
		$this->template->views('dashboard', $data);

	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */