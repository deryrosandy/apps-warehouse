<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General'));
	}

	public function index() {

		$this->kategori();
	}

	public function kategori() {

		$data['userdata'] 		= $this->userdata;	

		$data['kategoris'] = $this->M_General->get_all_category();

		$data['page'] 			= "kategori";
		$data['judul'] 			= "kategori";
		$data['deskripsi'] 		= "";
		
		$this->template->views('kategori/list_kategori', $data);
	}

	public function tambah_kategori() {
		
		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "kategori";
		$data['judul'] 			= " ";
		$data['deskripsi'] 		= " ";
		
		$this->template->views('kategori/tambah_kategori', $data);
	}

	public function insert_kategori() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$product_category_id = $this->input->post('product_category_id', TRUE);
		unset($data['product_category_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($product_category_id)) { 
					
					$result = $this->M_General->update_kategori($data, $product_category_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('kategori Berhasil Di Update'));
						$url = base_url() . 'kategori';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('kategori Gagal Di Update'));
						$url = base_url() . 'kategori';
						redirect($url);
						//header("Refresh:0");
					}

				}else{
					$result = $this->M_General->insert_kategori($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('kategori Berhasil Dimasukkan'));
						$url = base_url() . 'kategori';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('kategori Gagal Dimasukkan'));
						$url = base_url() . 'kategori';
						redirect($url);
						//header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('kategori/tambah_kategori', $data);
	
	}

	public function update_kategori($product_category_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['kategori'] = $this->M_General->get_kategori_by_id($product_category_id);

		$data['page'] 			= "kategori";
		$data['judul'] 			= "Update kategori";
		$data['deskripsi'] 		= "";
		
		$this->template->views('kategori/tambah_kategori', $data);
	
	}

	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_kategori($id);
	
		echo json_encode($result);
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */