<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General'));
	}

	public function index() {

		$this->list_vendor();
		
	}

	public function list_vendor() {

		$data['userdata'] 		= $this->userdata;	

		$data['vendors'] = $this->M_General->get_all_vendor();

		$data['page'] 			= "vendor";
		$data['judul'] 			= "Vendor";
		$data['deskripsi'] 		= "";
		
		$this->template->views('vendor/list_vendor', $data);
	}

	public function tambah_vendor() {
		
		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "vendor";
		$data['judul'] 			= " ";
		$data['deskripsi'] 		= " ";
		
		$this->template->views('vendor/tambah_vendor', $data);
	}

	public function insert_vendor() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$vendor_id = $this->input->post('vendor_id', TRUE);
		unset($data['vendor_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($vendor_id)) { 
					
					$result = $this->M_General->update_vendor($data, $vendor_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Vendor Berhasil Di Update'));
						$url = base_url() . 'vendor';
						redirect($url);
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Vendor Gagal Di Update'));
						$url = base_url() . 'vendor/list_vendor';
						redirect($url);
					}

				}else{
					$result = $this->M_General->insert_vendor($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Vendor Berhasil Dimasukkan'));
						$url = base_url() . 'vendor/list_vendor';
						redirect($url);
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Vendor Gagal Dimasukkan'));
						$url = base_url() . 'vendor/list_vendor';
						redirect($url);
					}
				
				}
			}
		}
		
		$this->template->views('vendor/tambah_vendor', $data);
	
	}

	public function update_vendor($vendor_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['vendor'] = $this->M_General->get_vendor_by_id($vendor_id);

		$data['page'] 			= "vendor";
		$data['judul'] 			= "Update Vendor";
		$data['deskripsi'] 		= "";
		
		$this->template->views('vendor/tambah_vendor', $data);
	
	}

	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_vendor($id);
	
		echo json_encode($result);
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */