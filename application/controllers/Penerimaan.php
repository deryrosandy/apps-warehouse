<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {
		$this->data_penerimaan();
	}

	public function data_penerimaan($report_month = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "data_penerimaan";
		$data['judul'] 			= "Data Penerimaan";
		$data['deskripsi'] 		= "";	

		if (!empty($report_month)){
			
			$flag = $this->input->post('flag', TRUE);

			$data['penerimaan'] = $this->M_General->get_all_penerimaan($report_month);
			$data['month'] = $report_month;

			$this->template->views('penerimaan/data_penerimaan', $data);

		}else{

			$report_month = $this->input->post('report_month', TRUE);

			$data['penerimaan'] = $this->M_General->get_all_penerimaan($report_month);      
			
			$this->template->views('penerimaan/data_penerimaan', $data);

		}
		
	}

	public function tambah_penerimaan() {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "tambah_penerimaan";
		$data['judul'] 			= "Tambah Penerimaan";
		$data['deskripsi'] 		= "";	

		$data['vendors'] = $this->M_General->get_all_vendor();   
		$data['products'] = $this->M_General->get_all_product();     
		$data['warehouse'] = $this->M_General->get_all_warehouse();     

		$this->template->views('penerimaan/tambah_penerimaan', $data);

	}
		
	public function insert_penerimaan() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$receiving_id = $this->input->post('receiving_id', TRUE);
		unset($data['receiving_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($receiving_id)) { 
					
					$result = $this->M_General->update_receiving($data, $receiving_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Update'));
						$url = base_url() . 'penerimaan';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Gagal Di Update'));
						$url = base_url() . 'penerimaan';
						redirect($url);
						header("Refresh:0");
					}

				}else{

					$insert = $this->M_General->insert_receiving($data);   

					if ($insert==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Masukkan'));
						$url = base_url() . 'penerimaan';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Gagal Di Masukkan'));
						$url = base_url() . 'penerimaan';
						redirect($url);
					}
				
				}
			}
		}
		
		$this->template->views('penerimaan/tambah_penerimaan', $data);
	
	}

	public function update_penerimaan($receiving_id) {

		$data['userdata'] 	= $this->userdata;	

		$data['receiving'] = $this->M_General->get_receiving_by_id($receiving_id);
		
		$data['vendors'] = $this->M_General->get_all_vendor();   
		$data['products'] = $this->M_General->get_all_product();     
		$data['warehouse'] = $this->M_General->get_all_warehouse();  
		
		$data['product_list'] = $this->M_General->get_product_by_receving_id($receiving_id);  

		$data['page'] 			= "update_penerimaan";
		$data['judul'] 			= "Update Penerimaan";
		$data['deskripsi'] 		= "";
		
		$this->template->views('penerimaan/tambah_penerimaan', $data);

	}
	
	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_penerimaan($id);
	
		echo json_encode($result);
	}

	public function approval($receiving_id = null, $report_month = null) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "approval_penerimaan";
		$data['judul'] 			= "Approval Penerimaan";
		$data['deskripsi'] 		= "";	

		if (!empty($report_month)){
			
			$flag = $this->input->post('flag', TRUE);

			$data['penerimaan'] = $this->M_General->get_all_penerimaan($report_month);
			$data['month'] = $report_month;

			$this->template->views('penerimaan/data_penerimaan', $data);

		}else{

			$report_month = $this->input->post('report_month', TRUE);

			$data['penerimaan'] = $this->M_General->get_all_penerimaan($report_month);      
			
			$this->template->views('penerimaan/data_penerimaan', $data);

		}
        
	}

	public function print_detail($receiving_id) {
		
		$data['receiving'] = $this->M_General->get_receiving_by_id($receiving_id);
		$data['product_list'] = $this->M_General->get_product_by_receving_id($receiving_id);  
		
		$this->pdf->load_view('penerimaan/print_detail_penerimaan', $data);
		$this->pdf->set_paper('A5', 'landscape');
		$this->pdf->render();
		$this->pdf->stream("print_detail_penerimaan_" . str_replace("-","", $receiving_id) . ".pdf", array("Attachment" => false));		

	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */