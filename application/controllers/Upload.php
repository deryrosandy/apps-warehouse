<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends AUTH_Controller {
 
 function __construct(){
     parent::__construct();
       $this->load->helper(array('form', 'url'));
 }

 public function process_upload(){
     $config['upload_path']          = './gambar/';
     $config['allowed_types']        = 'gif|jpg|png';
     $config['max_size']             = 100;
     $config['max_width']            = 1024;
     $config['max_height']           = 768;

     $this->load->library('upload', $config);

     if ( !$this->upload->do_upload('therapist_image')){
         $error = array('error' => $this->upload->display_errors());
         //$this->load->view('v_upload', $error);
     }else{
         $data = array('upload_data' => $this->upload->data());
         var_dump($data); die();
         //$this->load->view('v_upload_sukses', $data);
     }
 }
 
}