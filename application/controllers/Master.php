<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_admin', 'M_General'));
	}

	public function index() {

		$this->users();
	}

	public function users() {

		$data['userdata'] 		= $this->userdata;	

		$data['users'] = $this->M_General->get_all_user();

		$data['page'] 			= "users";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "users";
		
		$this->template->views('master/users', $data);
	
	}

	public function ajax_get_detail_user() {

		$id_user = $this->input->post('id_user');	

		$output = $this->M_General->get_user_by_id($id_user);
		
		echo json_encode($output);
	
	}

	public function vendor() {

		$data['userdata'] 		= $this->userdata;	

		$data['vendors'] = $this->M_General->get_all_vendor();

		$data['page'] 			= "vendor";
		$data['judul'] 			= "Vendor";
		$data['deskripsi'] 		= "";
		
		$this->template->views('master/list_vendor', $data);
	}
	
	public function supplier() {

		$data['userdata'] 		= $this->userdata;	

		$data['suppliers'] = $this->M_General->get_all_supplier();

		$data['page'] 			= "supplier";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "Supplier";
		
		$this->template->views('master/supplier', $data);
	
	}

	public function add_user() {

		$data = $this->input->post();	
		
		$data['status'] = ($data['status']=='on' ? '1':'0');
		$operation  =	$data['operation'];
		unset($data['operation']);
		$result = $this->M_General->insert_user($data);
	//	var_dump($result); die();
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil ditambah'));
			redirect('master/users');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal ditambah'));
			redirect('master/users');
		}
	}

	public function delete_user() {

		$id_user = $this->input->post('id_user');

		$result = $this->M_General->delete_user($id_user);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil ditambah'));
			redirect('master/users');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal ditambah'));
			redirect('master/users');
		}
	}

	public function update_user() {

		$data = $this->input->post();	
		$data['status'] = (@$data['status']=='on' ? '1':'0');
		$user_id  = $data['user_id'];
		$operation  =	$data['operation'];
		unset($data['user_id']);
		unset($data['operation']);
		if($data['password']==''){
			unset($data['password']);
		}
		
		$result = $this->M_General->update_user($user_id, $data);

		if ($result == true) {		
			redirect('master/users');
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil diubah'));
		} else {
			redirect('master/users');
			$this->session->set_flashdata('msg', show_err_msg('User Gagal diubah'));
		}
	}

	public function change_password() {
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
		
		$UserID = $this->userdata->UserID;
		
		if ($this->form_validation->run() == TRUE) {
			if (($this->input->post('passLama')) == $this->userdata->Passwd) {
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('profile');
				} else {
					$data = [
						'Passwd' => $this->input->post('passBaru')
					];
					
					$result = $this->M_admin->update($data, $UserID);
					
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('profile');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('profile');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */