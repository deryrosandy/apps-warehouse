<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengiriman extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {
		$this->data_pengiriman();
	}

	public function data_pengiriman($report_month = NULL) {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "data_pengiriman";
		$data['judul'] 			= "Data Pengiriman";
		$data['deskripsi'] 		= "";	

		if (!empty($report_month)){
			
			$flag = $this->input->post('flag', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_open($report_month);
			$data['month'] = $report_month;

			$this->template->views('pengiriman/data_pengiriman', $data);

		}else{

			$report_month = $this->input->post('report_month', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_open($report_month);      
			
			$this->template->views('pengiriman/data_pengiriman', $data);

		}
		
	}

	public function data_open($report_month = NULL) {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "data_open";
		$data['judul'] 			= "Data Pengiriman";
		$data['deskripsi'] 		= "";	

		if (!empty($report_month)){
			
			$flag = $this->input->post('flag', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_open($report_month);
			$data['month'] = $report_month;

			$this->template->views('pengiriman/data_pengiriman', $data);

		}else{

			$report_month = $this->input->post('report_month', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_open($report_month);      
			
			$this->template->views('pengiriman/data_pengiriman', $data);

		}
		
	}

	public function data_proses($report_month = NULL) {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "data_proses";
		$data['judul'] 			= "Data Pengiriman";
		$data['deskripsi'] 		= "";	

		if (!empty($report_month)){
			
			$flag = $this->input->post('flag', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_process($report_month);
			$data['month'] = $report_month;

			$this->template->views('pengiriman/data_pengiriman', $data);

		}else{

			$report_month = $this->input->post('report_month', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_process($report_month);      
			
			$this->template->views('pengiriman/data_pengiriman', $data);

		}
		
	}

	public function data_selesai($report_month = NULL) {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "data_selesai";
		$data['judul'] 			= "Data Pengiriman";
		$data['deskripsi'] 		= "";	

		if (!empty($report_month)){
			
			$flag = $this->input->post('flag', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_selesai($report_month);
			$data['month'] = $report_month;

			$this->template->views('pengiriman/data_pengiriman', $data);

		}else{

			$report_month = $this->input->post('report_month', TRUE);

			$data['pengiriman'] = $this->M_General->get_all_pengiriman_selesai($report_month);      
			
			$this->template->views('pengiriman/data_pengiriman', $data);

		}
		
	}

	public function tambah_pengiriman() {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "tambah_pengiriman";
		$data['judul'] 			= "Tambah Pengiriman";
		$data['deskripsi'] 		= "";	

		$data['customers'] = $this->M_General->get_customer_name();   
		$data['products'] = $this->M_General->get_all_product();     
		$data['warehouse'] = $this->M_General->get_all_warehouse();     
		$data['no_do'] = $this->M_General->get_new_no_do();

		$this->template->views('pengiriman/tambah_pengiriman', $data);

	}

	public function insert_pengiriman() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$shipment_id = $this->input->post('shipment_id', TRUE);
		unset($data['shipment_id']);

		$flag = $this->input->post('flag', TRUE);

		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($shipment_id)) { 
					
					$result = $this->M_General->update_shipment($data, $shipment_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Update'));
						$url = base_url() . 'pengiriman';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Gagal Di Update'));
						$url = base_url() . 'pengiriman';
						redirect($url);
						header("Refresh:0");
					}

				}else{

					$insert = $this->M_General->insert_shipment($data);   

					if ($insert==true) {
						$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Masukkan'));
						$url = base_url() . 'pengiriman';
						redirect($url);
						header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Gagal Di Masukkan'));
						$url = base_url() . 'pengiriman';
						redirect($url);
					}
				
				}
			}
		}

		$this->template->views('pengiriman/tambah_pengiriman', $data);

	}

	public function update_pengiriman($shipment_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['customers'] = $this->M_General->get_all_customer();   
		$data['products'] = $this->M_General->get_all_product();     
		$data['warehouse'] = $this->M_General->get_all_warehouse();  
		
		$data['product_list'] = $this->M_General->get_product_by_shipment_id($shipment_id);  

		$data['shipment'] = $this->M_General->get_shipment_by_id($shipment_id);

		$data['page'] 			= "pengiriman";
		$data['judul'] 			= "Update Pengiriman";
		$data['deskripsi'] 		= "";
		
		$this->template->views('pengiriman/tambah_pengiriman', $data);
	
	}

	public function approval() {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "approval_pengiriman";
		$data['judul'] 			= "Approval Pengiriman";
		$data['deskripsi'] 		= "";	

		$data['pengiriman'] = $this->M_General->get_all_pengiriman_approval();      
		
		$this->template->views('pengiriman/approval_pengiriman', $data);

	}

	public function approval_pengiriman($shipment_id) {

		$data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "approval_pengiriman";
		$data['judul'] 			= "Approval Pengiriman";
		$data['deskripsi'] 		= "";	

		$data['userdata'] 		= $this->userdata;	

		$data['customers'] = $this->M_General->get_all_customer();   
		$data['products'] = $this->M_General->get_all_product();     
		$data['warehouse'] = $this->M_General->get_all_warehouse();  
		
		$data['product_list'] = $this->M_General->get_product_by_shipment_id($shipment_id);  

		$data['shipment'] = $this->M_General->get_shipment_by_id($shipment_id);

		$shipment_id = $this->input->post('shipment_id', TRUE);
		$set_process = $this->input->post();

		$set_process 	= $this->input->post('set_process', TRUE);
		$set_completed 	= $this->input->post('set_completed', TRUE);
		///var_dump($set_process); die();
		if (!empty($set_process)) { 
			$data_update['status'] = 'process';
			$data_update['approved'] = 1;
			$result = $this->M_General->approval_shipment($data_update, $shipment_id);     

			if ($result==true) {
				$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Update'));
				$url = base_url() . 'pengiriman/approval_pengiriman/' . $shipment_id;
				redirect($url);
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Gagal Di Update'));
				$url = base_url() . 'pengiriman/approval_pengiriman/' . $shipment_id;
				redirect($url);
			}
		}elseif(!empty($set_completed)){
			$data_update['status'] = 'done';
			$data_update['approved'] = 1;
			$result = $this->M_General->approval_shipment($data_update, $shipment_id);     

			if ($result==true) {
				$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Update'));
				$url = base_url() . 'pengiriman/approval_pengiriman/' . $shipment_id;
				redirect($url);
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Gagal Di Update'));
				$url = base_url() . 'pengiriman/approval_pengiriman/' . $shipment_id;
				redirect($url);
			}
		}
		
		$this->template->views('pengiriman/detail_pengiriman', $data);
	
	}

	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_pengiriman($id);
	
		echo json_encode($result);
	}

	public function print_detail($shipment_id) {
		
		$data['shipment'] = $this->M_General->get_shipment_by_id($shipment_id);
		$data['product_list'] = $this->M_General->get_product_by_shipment_id($shipment_id);  

		$this->pdf->load_view('pengiriman/print_detail_pengiriman', $data);
		$this->pdf->set_paper('A5', 'landscape');
		$this->pdf->render();
		$this->pdf->stream("print_detail_pengiriman_" . str_replace("-","", $shipment_id) . ".pdf", array("Attachment" => false));		

	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */