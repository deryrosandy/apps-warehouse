<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {
		$this->barang_diterima();
	}

	public function barang_diterima($start_date = NULL, $end_date = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "laporan_barang_diterima";
		$data['judul'] 			= "Laporan Barang Di Terima";
		$data['deskripsi'] 		= "";	

		
			
		$flag = $this->input->post('flag', TRUE);

		$start_date = $this->input->post('start_date', TRUE);
		$end_date 	= $this->input->post('end_date', TRUE);

		if (!empty($flag) && !empty($start_date && !empty($end_date))) { 

			$data['flag'] = 1;
			if (!empty($start_date)) {
				$data['start_date'] = $start_date;
			} else {
				$data['start_date'] = $this->input->post('start_date', TRUE);
			}  

			if (!empty($end_date)) {
				$data['end_date'] = $end_date;
			} else {
				$data['end_date'] = $this->input->post('end_date', TRUE);
			}  

			$data['receiving_list'] = $this->M_General->get_receiving_list_by_date($start_date, $end_date); 
			
			///$this->template->views('laporan/print_laporan_barang_diterima', $data);
			$this->pdf->load_view('laporan/print_laporan_barang_diterima', $data);
			$this->pdf->set_paper('A4', 'landscape');
			$this->pdf->render();
			$this->pdf->stream("laporan_barang_diterima_" . str_replace("-","", $data['start_date']), array("Attachment" => false));		                 
		}else{
			$this->template->views('laporan/laporan_barang_diterima', $data);
		}	
        
	}

	public function barang_dikirim($start_date = NULL, $end_date = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "laporan_barang_dikirim";
		$data['judul'] 			= "Laporan Barang Di Kirim";
		$data['deskripsi'] 		= "";	
			
		$flag = $this->input->post('flag', TRUE);

		$start_date = $this->input->post('start_date', TRUE);
		$end_date 	= $this->input->post('end_date', TRUE);

		if (!empty($flag) && !empty($start_date && !empty($end_date))) { 

			$data['flag'] = 1;
			if (!empty($start_date)) {
				$data['start_date'] = $start_date;
			} else {
				$data['start_date'] = $this->input->post('start_date', TRUE);
			}  

			if (!empty($end_date)) {
				$data['end_date'] = $end_date;
			} else {
				$data['end_date'] = $this->input->post('end_date', TRUE);
			}  

			$data['shipment_list'] = $this->M_General->get_shipment_list_by_date($start_date, $end_date); 
			
			///$this->template->views('laporan/print_laporan_barang_diterima', $data);
			$this->pdf->load_view('laporan/print_laporan_barang_dikirim', $data);
			$this->pdf->set_paper('A4', 'landscape');
			$this->pdf->render();
			$this->pdf->stream("laporan_barang_dikirim_" . str_replace("-","", $data['start_date']), array("Attachment" => false));		                 
		}else{
			$this->template->views('laporan/laporan_barang_dikirim', $data);
		}	
        
	}

	public function stock_barang($category_id = NULL) {

        $data['userdata'] 		= $this->userdata;		
		$data['page'] 			= "laporan_stock_barang";
		$data['judul'] 			= "Laporan Stock Barang";
		$data['deskripsi'] 		= "";	
			
		$flag = $this->input->post('flag', TRUE);
		$category_id = $this->input->post('category_id', TRUE);

		$data['categories'] = $this->M_General->get_all_product_category();

		if (!empty($flag) && !empty($category_id)){ 

			$data['flag'] = 1;
			
			if (!empty($category_id)) {
				$data['category_id'] = $category_id;
			} else {
				$data['category_id'] = $this->input->post('category_id', TRUE);
			}  

			$data['product_stock'] = $this->M_General->get_product_stock_by_category($category_id); 
			
			///$this->template->views('laporan/print_laporan_barang_diterima', $data);
			$this->pdf->load_view('laporan/print_stock_barang', $data);
			$this->pdf->set_paper('A4', 'portrait');
			$this->pdf->render();
			$this->pdf->stream("laporan_stock_barang_" . str_replace("-","", $data['category_id']), array("Attachment" => false));	

		}else{

			$this->template->views('laporan/laporan_stock_barang', $data);
		
		}	
        
	}

}