<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General'));
	}

	public function index() {

		$this->warehouse();
	}

	public function warehouse() {

		$data['userdata'] 		= $this->userdata;	

		$data['warehouses'] = $this->M_General->get_all_warehouse();

		$data['page'] 			= "warehouse";
		$data['judul'] 			= "warehouse";
		$data['deskripsi'] 		= "";
		
		$this->template->views('warehouse/list_warehouse', $data);
	}

	public function tambah_warehouse() {
		
		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "warehouse";
		$data['judul'] 			= " ";
		$data['deskripsi'] 		= " ";
		
		$this->template->views('warehouse/tambah_warehouse', $data);
	}

	public function insert_warehouse() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$warehouse_id = $this->input->post('warehouse_id', TRUE);
		unset($data['warehouse_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($warehouse_id)) { 
					
					$result = $this->M_General->update_warehouse($data, $warehouse_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('warehouse Berhasil Di Update'));
						$url = base_url() . 'warehouse';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('warehouse Gagal Di Update'));
						$url = base_url() . 'warehouse';
						redirect($url);
						//header("Refresh:0");
					}

				}else{
					$result = $this->M_General->insert_warehouse($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('warehouse Berhasil Dimasukkan'));
						$url = base_url() . 'warehouse';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('warehouse Gagal Dimasukkan'));
						$url = base_url() . 'warehouse';
						redirect($url);
						//header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('warehouse/tambah_warehouse', $data);
	
	}

	public function update_warehouse($warehouse_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['warehouse'] = $this->M_General->get_warehouse_by_id($warehouse_id);

		$data['page'] 			= "warehouse";
		$data['judul'] 			= "Update warehouse";
		$data['deskripsi'] 		= "";
		
		$this->template->views('warehouse/tambah_warehouse', $data);
	
	}

	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_warehouse($id);
	
		echo json_encode($result);
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */