<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General'));
	}

	public function index() {

		$this->users();
	}

	public function users() {

		$data['userdata'] 		= $this->userdata;	

		$data['users'] = $this->M_General->get_all_user();

		$data['page'] 			= "users";
		$data['judul'] 			= "users";
		$data['deskripsi'] 		= "";
		
		$this->template->views('users/list_user', $data);
	}

	public function tambah_user() {

		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "tambah_user";
		$data['judul'] 			= "Tambah User";
		$data['deskripsi'] 		= "";
		
		$this->template->views('users/tambah_user', $data);
	
	}

	public function insert_user() {

		$data['userdata'] 		= $this->userdata;	
		$data = $this->input->post();
		$data['active'] = (@$data['active']=='on' ? '1':'0');
		$user_id = $this->input->post('user_id', TRUE);
		unset($data['user_id']);

		$flag = $this->input->post('flag', TRUE);
		
		if ($flag == 'flag'){

			if (!empty($flag)) { 

				if (!empty($user_id)) { 
					
					$result = $this->M_General->update_user($data, $user_id);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('User Berhasil Di Update'));
						$url = base_url() . 'users';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('User Gagal Di Update'));
						$url = base_url() . 'users';
						redirect($url);
						//header("Refresh:0");
					}

				}else{
					$result = $this->M_General->insert_user($data);     

					if ($result==true) {
						$this->session->set_flashdata('msg', show_succ_msg('users Berhasil Dimasukkan'));
						$url = base_url() . 'users';
						redirect($url);
						//header("Refresh:0");
					} else {
						$this->session->set_flashdata('msg', show_err_msg('users Gagal Dimasukkan'));
						$url = base_url() . 'users';
						redirect($url);
						//header("Refresh:0");
					}
				
				}
			}
		}
		
		$this->template->views('users/tambah_user', $data);
	
	}

	public function update_user($users_id) {

		$data['userdata'] 		= $this->userdata;	

		$data['user'] = $this->M_General->get_user_by_id($users_id);

		$data['page'] 			= "users";
		$data['judul'] 			= "Update User";
		$data['deskripsi'] 		= "";
		
		$this->template->views('users/tambah_user', $data);
	
	}

	public function delete() {

		$id = $this->input->post('id');

		$result = $this->M_General->delete_user($id);
	
		echo json_encode($result);
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */